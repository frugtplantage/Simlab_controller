#include <ESP8266WiFi.h>
#include <PubSubClient.h>
//Define what unit
const String espID = "meter";

// WiFi
const char *ssid = "----"; // WiFi name
const char *password = "----";  //  WiFi password
// MQTT Broker
const char *mqttBroker = "----"; // broker ip conneciton
const int mqttPort = 1883;

const char *topic1 = "kys/esp/meter";
const char *topic2 = "kys/esp/prod1";
const char *topic3 = "kys/esp/coolerRoom";
const char *topic4 = "kys/pyt/coolerCommand";
const char *topic5 = "kys/pyt/prod1Command";




int sensorPin = A0;

int threshold = 800;
bool activePulse = false;
int pulseCount = 0;
int numPulse = 10;
unsigned long timeCount = millis();
unsigned long timeNow = millis();
int waitPeriod = 10*60*1000; //first digit is desired # minutes

float deltaT = 0;
float perHour = 0;
float sumPower = 0;
int sumCount = 0;


WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
 Serial.begin(115200);
 pinMode(sensorPin, INPUT);
 
 // connect to WiFi network
 WiFi.begin(ssid, password);
 while (WiFi.status() != WL_CONNECTED) {
  delay(500);
  Serial.println("Connecting to WiFi..");
 }
 
 Serial.println("Connected to the WiFi network");
 
 //connecting to a mqtt broker
 client.setServer(mqttBroker, mqttPort);
 client.setCallback(callback);
 
 while (!client.connected()) {
 String client_id = "esp8266-client-";
 client_id += espID;  //String(WiFi.macAddress());
 
 Serial.printf("The client %s connects to mosquitto mqtt broker\n", client_id.c_str());
 
 if (client.connect(client_id.c_str())) {
  Serial.println("MQTT broker connected");
 } else {
  Serial.print("failed with state ");
  Serial.print(client.state());
  delay(2000);
 }
}

 // publish and subscribe
 String starting = "Hello from ";
 starting += espID;
 starting += " ESP!";
 //client.publish(topic1, starting.c_str());  //debug message, remove when not needed to avoid any errors with reading
}

//callback function for MQTT
void callback(char* topic, byte* payload, unsigned int length) {
 Serial.print("Message arrived in topic: ");
 Serial.println(topic);
 Serial.print("Message:");
 
 for (int i = 0; i < length; i++) {
  Serial.print((char) payload[i]);
 }
 
 Serial.println();
 Serial.println(" - - - - - - - - - - - -");
}


void loop() {
 client.loop();

 int sensorValue = analogRead(sensorPin);
 delay(5);
 //Serial.println(sensorValue);

 if (sensorValue < threshold){
  activePulse = false;
 } else if (sensorValue >= threshold and activePulse == false){
  activePulse = true;
  pulseCount += 1;

  if (pulseCount >= numPulse){
    pulseCount = 0;
    deltaT = millis()-timeCount;

    perHour = (numPulse/deltaT)*60*60;
  
    timeCount = millis();
    Serial.print("kWh use: ");
    Serial.print(perHour);

    sumPower += perHour;
    sumCount += 1;

    //String STRperHour = String(perHour, 2);
    //client.publish(topic1, STRperHour.c_str());
  }
 }


 if (millis()-timeNow >= waitPeriod) {
    timeNow = millis();
    if (sumCount == 0){ //return 0 kWh if doesnt read anything
      sumCount = 1;
    }
    String result = String(sumPower/sumCount); //avg over the past 10 min
    client.publish(topic1, result.c_str());
    Serial.println(result);
    Serial.println(sumPower/sumCount);
    sumPower = 0;
    sumCount = 0;
    
   }

WiFiReconnect();
MQTTReconnect();
}


void WiFiReconnect(){
  if (WiFi.status() != WL_CONNECTED){ //wifi lost
    Serial.println("WiFi disconnected, attempting to reconnect..");
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.println("Connecting to WiFi..");
    }
    Serial.println("Connected to the WiFi network");
  }
}

void MQTTReconnect(){ 
 if(!client.connected()){

   Serial.println("MQTT disconnected, attempting to reconnect..");
  


   while (!client.connected()) {
      String client_id = "esp8266-client-";
      client_id += String(WiFi.macAddress());
 
      Serial.printf("The client %s connects to mosquitto mqtt broker\n", client_id.c_str());
 
      if (client.connect(client_id.c_str())) {
          Serial.println("local mqtt broker connected");
          
          client.subscribe(topic);//reconnect subscribes
      } else {
          Serial.print("failed with state ");
          Serial.println(client.state());
          WiFiReconnect(); //ensure we aren't failing because of failed internet
          delay(2000);
      }
   }
   
 }
  
}
