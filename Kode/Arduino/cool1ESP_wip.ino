#include <ESP8266WiFi.h>
#include <PubSubClient.h>
//Define what unit
const String espID = "cool1";

// WiFi
const char *ssid = "----"; // WiFi name
const char *password = "----";  //  WiFi password
// MQTT Broker
const char *mqttBroker = "----"; // broker ip conneciton
const int mqttPort = 1883;

const char *topic1 = "kys/esp/meter";
const char *topic2 = "kys/esp/prod1";
const char *topic3 = "kys/esp/coolerRoom";
const char *topic4 = "kys/pyt/coolerCommand";
const char *topic5 = "kys/pyt/prod1Command";


unsigned long timeNow = millis();
int waitPeriod = 10*60*1000; //first digit is # minutes

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
 Serial.begin(115200);
 
 // connect to WiFi network
 WiFi.begin(ssid, password);
 while (WiFi.status() != WL_CONNECTED) {
  delay(500);
  Serial.println("Connecting to WiFi..");
 }
 
 Serial.println("Connected to the WiFi network");
 
 //connecting to a mqtt broker
 client.setServer(mqttBroker, mqttPort);
 client.setCallback(callback);
 
 while (!client.connected()) {
 String client_id = "esp8266-client-";
 client_id += espID;  //String(WiFi.macAddress());
 
 Serial.printf("The client %s connects to mosquitto mqtt broker\n", client_id.c_str());
 
 if (client.connect(client_id.c_str())) {
  Serial.println("MQTT broker connected");
 } else {
  Serial.print("failed with state ");
  Serial.print(client.state());
  delay(2000);
 }
}

 // publish and subscribe
 String starting = "Hello from ";
 starting += espID;
 starting += " ESP!";
 //client.publish(topic3, starting.c_str()); //debug message, remove when not needed to avoid any errors with reading
}

//callback function for MQTT
void callback(char* topic, byte* payload, unsigned int length) {
  //insert modbus function, to write or read whatever specified value comes in from MQTT

  //test further with message handling 
}

void loop() {
 client.loop();


 if (millis()-timeNow >= waitPeriod) {
    timeNow = millis();

    //modbus function, to read the value regularly, ie current temp, and maybe one or two others, like current setpoint other status?
    //publish result of modbus to topic2
    //client.publish(topic3, result.c_str());

    //test publish
    String result = String(random(1001,2000));
    client.publish(topic3, result.c_str());
    
   }

WiFiReconnect();
MQTTReconnect();
}

void WiFiReconnect(){
  if (WiFi.status() != WL_CONNECTED){ //wifi lost
    Serial.println("WiFi disconnected, attempting to reconnect..");
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.println("Connecting to WiFi..");
    }
    Serial.println("Connected to the WiFi network");
  }
}

void MQTTReconnect(){ 
 if(!client.connected()){

   Serial.println("MQTT disconnected, attempting to reconnect..");
  


   while (!client.connected()) {
      String client_id = "esp8266-client-";
      client_id += String(WiFi.macAddress());
 
      Serial.printf("The client %s connects to mosquitto mqtt broker\n", client_id.c_str());
 
      if (client.connect(client_id.c_str())) {
          Serial.println("local mqtt broker connected");
          
          client.subscribe(topic);//reconnect subscribes
      } else {
          Serial.print("failed with state ");
          Serial.println(client.state());
          WiFiReconnect(); //ensure we aren't failing because of failed internet
          delay(2000);
      }
   }
   
 }
  
}
