import paho.mqtt.client as mqtt
from datetime import datetime
import csv

fileName = 'MeterData.csv'


topic1 = "kys/esp/meter"
topic2 = "kys/esp/prod1"
topic3 = "kys/esp/coolerRoom"
topic4 = "kys/pyt/coolerCommand"
topic5 = "kys/pyt/prod1Command"

line = 0 #start at 0 because our header is 0 (not real data)
sensor_data = []
time_data = []


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(topic1)

# The callback for when a PUBLISH message is received from the server.





def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    if (msg.topic == topic1):
        val = float(msg.payload)
        sensor_data.append(val)

        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        time_data.append(dt_string)


        if (len(sensor_data)%2 == 0):
            #print(sensor_data)
            #print(time_data)
            final = list(zip(time_data, sensor_data))

            #print(final)
            file = open(fileName, "a")
            with open(fileName, 'a', encoding='UTF8', newline='') as f:
                writer = csv.writer(f)
                writer.writerows(final)
            file.close()

            sensor_data.clear()
            time_data.clear()
            final.clear()




client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect('----', 1883, 60) #replace ---- with local broker ip

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()

