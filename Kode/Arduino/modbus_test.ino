#include <ModbusRTU.h>
ModbusRTU mb;

#include <SoftwareSerial.h>
int DE_RE = D4; //D4  For MAX485 chip
int RX = D6;
int TX = D7;


SoftwareSerial S(RX, TX);//D6/D7  (RX , TX)

uint16_t Mread0[2];

bool cbWrite(Modbus::ResultCode event, uint16_t transactionId, void* data) {
  Serial.printf_P("Request result: 0x%02X, Mem: %d\n", event, ESP.getFreeHeap());
  return true;
}

void setup() {
  Serial.begin(115200);
  S.begin(9600, SWSERIAL_8N1);
  mb.begin(&S, DE_RE);
  mb.master();
  Serial.println(); //Print empty line
  Serial.println(sizeof(Mread0)); //Reaing size of first array
}
void loop() {
  if (!mb.slave()) { //036 ModbusID for lille kølerum, 1582 adresse for nuværende setpoint
    mb.readHreg(036, 1582, Mread0, 2 , cbWrite);  //(SlaveID,Address,Buffer,Range of data,Modus call);
    Serial.println(InttoFloat(Mread0[0],Mread0[1]));
  }
  mb.task();
  delay(1000);
  yield();
}

float InttoFloat(uint16_t Data0,uint16_t Data1) {
  float x;
  unsigned long *p;
  p = (unsigned long*)&x;
  *p = (unsigned long)Data0 << 16 | Data1; //Big-endian
  return(x);
}
