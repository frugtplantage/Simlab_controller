import parameters
from datetime import datetime
from time import sleep
import paho.mqtt.client as mqtt

pow_time = datetime.now()
def on_message(client, userdata, message):
    #client.publish(f"{parameters.ev_heartbeat_1}",chrg)
    global forecast_val, forecast_time, head_permission, head_permission_time, forecast

    if message.topic == parameters.head_port_usage_topic_1:
        global pow, pow_time
        pow = float(message.payload.decode("utf-8"))
        pow_time = datetime.now()
        print(pow_time)
    sleep(0.5)

'''
def on_message(client, userdata, message):
    client.publish(f"{parameters.freezer_heartbeat_1}", cool)
    if message.topic == parameters.freezer1_topic_1:
        global message_received
        message_received = float(message.payload.decode("utf-8"))
    if message.topic == parameters.head_port_forecast_topic_1:
        global nextPow
        nextPow = float(message.payload.decode("utf-8"))
    if message.topic == parameters.head_port_usage_topic_1:
        global pow, pow_time
        pow = float(message.payload.decode("utf-8"))
        pow_time = datetime.now()
        print(pow_time)
    if message.topic == parameters.freezer_pow_answer_1:
        global head_permission
        global head_permission_time
        head_permission = float(message.payload.decode("utf-8"))
        head_permission_time = datetime.now()
    sleep(0.3)
'''

mqttBroker ="mqtt.eclipseprojects.io"
client = mqtt.Client("evCharger_2")
client.connect(mqttBroker)
client.subscribe(parameters.head_port_usage_topic_1)
#client.subscribe(parameters.head_port_full_forecast_topic_1)
client.subscribe(parameters.head_port_full_forecast_time_topic_1)
client.subscribe(parameters.ev_pow_answer_1)
client.loop_start()
client.on_message = on_message

'''
client = mqtt.Client("freezer_Sim_1")
client.connect(mqttBroker)
client.subscribe(parameters.head_port_usage_topic_1)
client.subscribe(parameters.freezer1_topic_1)
client.subscribe(parameters.head_port_forecast_topic_1)
client.subscribe(parameters.freezer_pow_answer_1)
client.loop_start()
client.on_message = on_message
'''
while True:
    sleep(10)
