import requests
from datetime import datetime, timedelta
import pandas as pd
from time import sleep

def addDateInfo(dataframe, dateColumn = 'Fra_dato'):
    try:
        dataframe['month'] = dataframe[dateColumn].apply(lambda x: x.month)
        dataframe['weekday'] = dataframe[dateColumn].apply(lambda x: x.weekday())
        dataframe['hour'] = dataframe[dateColumn].apply(lambda x: x.hour)
    except:
        for i,row in dataframe.iterrows():
            dataframe.at[i,'month'] = i.month
            dataframe.at[i,'weekday'] = i.weekday()
            dataframe.at[i,'hour'] = i.hour
    return dataframe

def getIns (timeframe = '2022-03-28T22:00:00Z/2023-03-26T21:00:00Z'):

    api_key = '1fcf1c11-b12a-4374-8272-10195810aa0d'
    url = f'https://dmigw.govcloud.dk/v2/climateData/collections/10kmGridValue/items?cellId=10km_613_66&datetime={timeframe}&limit=250000&api-key={api_key}&parameterId=mean_radiation'
    r = requests.get(url)
    data = r.json()
    #print(data['features'])
    insval = []
    insdate = []
    for i in range(len(data['features'])):
        insval.append(data['features'][i]['properties']['value'])
        insdate.append(data['features'][i]['properties']['from'])
    insolation = pd.DataFrame({'insolation': insval, 'Fra_dato': insdate})

    insolation.drop(insolation.tail(1).index, inplace=True)
    insolation['Fra_dato'] = pd.to_datetime(insolation['Fra_dato'], format='%Y-%m-%d %H:%M:%S', utc=True)

    #insolation['Fra_dato'] = insolation['Fra_dato'].dt.to_timestamp(errors='coerce')
    #insolation['Fra_dato'] = insolation['Fra_dato'].dt.round("H")

    return insolation

def getMetObsIns (startTime,endTime = '..'):
    api_key = '22cbd0a9-83ed-4fdd-b022-c39b9e1a3bf2'
    # time format 2022-03-28T22:00:00Z
    time = startTime+ '/'+endTime
    url = f'https://dmigw.govcloud.dk/v2/metObs/collections/observation/items?stationId=06135&api-key={api_key}&datetime={time}&parameterId=radia_glob'
    r = requests.get(url)
    data = r.json()
    insval = []
    insdate = []
    for i in range(len(data['features'])):
        insval.append(data['features'][i]['properties']['value'])
        insdate.append(data['features'][i]['properties']['observed'])
    insolation = pd.DataFrame({'insolation': insval, 'Fra_dato': insdate})

    insolation['Fra_dato'] = pd.to_datetime(insolation['Fra_dato'], format='%Y-%m-%d %H:%M:%S', utc=True)
    insolation = addDateInfo(insolation)
    insolation = insolation.set_index('Fra_dato')
    return insolation

def getForecastIns():
    r = requests.get(
        "https://dmigw.govcloud.dk/v1/forecastedr/collections/harmonie_nea_sf/position?coords=POINT%2811.627%2055.289%29&crs=native&parameter-name=global-radiation&api-key=9ad9fca4-58e9-42d4-aa69-c05149a86eb5")
    data = r.json()
    insval = data['ranges']['global-radiation']['values']
    insdate = data['domain']['axes']['t']['values']
    insolation = pd.DataFrame({'insolation': insval, 'Fra_dato': insdate})
    # Converting to W/m^2
    insolation['insolation'] = insolation['insolation'] / (60 * 60)
    insolation['insolation'] = insolation['insolation'] - insolation['insolation'].shift()
    insolation['insolation'] = insolation['insolation'].fillna(0)

    insolation['Fra_dato'] = pd.to_datetime(insolation['Fra_dato'], format='%Y-%m-%d %H:%M:%S', utc=True)

    insolation = addDateInfo(insolation)
    insolation = insolation.set_index('Fra_dato')
    return insolation

def extractID (df, ID):
    df = df[df.MålepunktsID == ID]
    df = df.reset_index(drop=True)
    return df

def prepData(filename):
    path = fr"C:\Users\Asmus\PycharmProjects\Simlab_controller\Kode\Simlab\data\training_data\{filename}"
    ''' '2022-03-31T22:00:00Z/2023-03-26T21:00:00Z'
    script_dir = os.path.dirname(__file__)
    rel_path_consump = "data\\training_data\\Meterdata_forbrug_20230329.ods"
    abs_path_consump = os.path.join(script_dir, rel_path_consump)
    '''
    df_consump = pd.read_excel(io=path)
    # df_consumpid1 = extractID(df_consump, 571313174000874957), salg til elnet
    # df_consumpid2 = extractID(df_consump, 571313174000990121), forbrug
    # df_consumpid3 = extractID(df_consump, 571313181100727113), samme som id2
    df_prod = extractID(df_consump, 571313174000874957)
    df_consump = extractID(df_consump, 571313174000990121)

    # Adding production of which approx 60% is exported
    df_prod["Mængde"] = df_prod["Mængde"] * (-1)
    df_consump["Mængde"] = pd.concat([df_consump.Mængde, df_prod.Mængde], axis=1).fillna(0).sum(axis=1)

    df_consump['Fra_dato'] = pd.to_datetime(df_consump['Fra_dato'], format='%Y-%m-%d %H:%M:%S')
    df_consump['Fra_dato'] = df_consump['Fra_dato'].dt.round("H")
    # Converting to UTC
    df_consump['Fra_dato'] = pd.to_datetime(df_consump['Fra_dato']).dt.tz_localize('Europe/Copenhagen',
                                                                                   ambiguous='infer').dt.tz_convert(
        'UTC')

    # Adding insolation column
    timeStart = df_consump.head(1)['Fra_dato'][0]
    timeEnd = df_consump.tail(1)['Fra_dato'][len(df_consump)-1]
    timeStart = timeStart.strftime("%Y-%m-%dT%H:%M:00Z")
    timeEnd = timeEnd.strftime("%Y-%m-%dT%H:%M:00Z")
    timeframe = timeStart + '/' + timeEnd
    try:
        insolation = getIns(timeframe=timeframe)
        df_consump = df_consump.merge(insolation, how='left')
    except:
        for i in range(10):
            print("ERROR, CANNOT ACCESS DMI API")
            print("SETTING INSOLATION TO 0, FORECAST PERFORMANCE WILL BE REDUCED")
        df_consump['insolation'] = df_consump['Mængde']*0

    df_consump = df_consump.drop(columns=['MålepunktsID', 'Type', 'Måleenhed', 'Kvalitet', 'Til_dato'])
    df_consump = df_consump.rename(columns={'Mængde' : 'amount'})
    df_consump['month'] = df_consump['Fra_dato'].dt.month
    df_consump['weekday'] = df_consump["Fra_dato"].dt.dayofweek
    df_consump['hour'] = df_consump['Fra_dato'].dt.hour
    df_consump = df_consump.set_index('Fra_dato')
    df_consump = df_consump.asfreq('H')
    df_consump = df_consump.sort_index()
    df_consump = df_consump.fillna(0)

    df_consump['insolation'] = df_consump['insolation'].astype('category')
    df_consump['month'] = df_consump['month'].astype('category')
    df_consump['weekday'] = df_consump['weekday'].astype('category')
    return df_consump

def getTariff(hour):
    if hour < 6:
        tariff = 0.15090
    elif hour < 17:
        tariff = 0.42670
    elif hour < 21:
        tariff = 1.28020
    else:
        tariff = 0.42670
    return tariff

def getPrices(init = False):
    now = datetime.now()
    if init:
        currdate = now.strftime("%Y-%m-%dT00:00")
        tomorrow = now + timedelta(days=1)
        stopdate = tomorrow.strftime("%Y-%m-%dT00:00")
    else:
        currdate = now.strftime("%Y-%m-%dT%H:00")
        #currdate = now.strftime("%Y-%m-%dT23:00")
        tomorrow = now + timedelta(days=2)
        stopdate = tomorrow.strftime("%Y-%m-%dT00:00")
    url = f'https://api.energidataservice.dk/dataset/Elspotprices?offset=0&start={currdate}&end={stopdate}&filter=%7B%22PriceArea%22:[%22DK2%22]%7D&sort=HourUTC%20DESC&timezone=dk'
    req = requests.get(url)
    prices = req.json()
    # Processing data
    df = pd.DataFrame(prices['records'])
    df = df.drop(columns=['PriceArea', 'SpotPriceEUR'])
    df['HourUTC'] = pd.to_datetime(df['HourUTC'])
    df['HourDK'] = pd.to_datetime(df['HourDK'])
    df = df.set_index('HourUTC')
    # Converting to DKK/kWh
    df['SpotPriceDKK'] = df['SpotPriceDKK'] / 1000
    for i, row in df.iterrows():
        df.at[i,'SpotPriceDKK'] = df.loc[i][1]+getTariff(df.loc[i][0].hour)
    return df

# Removes the net tariffs from cost if using/expecting surplus power
def priceAt(df, hourDelta = 0, netPower = 0, timeZone = 'HourDK'):
    currTime = datetime.now().replace(minute=0, second=0, microsecond=0)
    if timeZone == 'HourDK':
        price = df.loc[currTime + timedelta(hours=hourDelta)][0]
    else:
        print('Timezone not programmed')
    if netPower < 0:
        price = price - getTariff(currTime.hour)
    return price


'''
def on_message(client, userdata, message):

    global message_received
    sleep(1)
    message_received=str(message.payload.decode("utf-8"))
    print(f'abd: {message_received}')
'''
