import pandas

from util import prepData
import pandas as pd
import numpy as np

def sign(number):
    if number<0:
        return -1
    else:
        return 1

def isSignCorrect(number1, number2):
    if sign(number1) == -1 and sign(number2) == -1:
        return -1
        # True negative
    elif sign(number1) == 1 and sign(number2) == 1:
        return 1
        # True positive
    elif sign(number1)-sign(number2) == 2:
        return 2
        # False positive
    elif sign(number1)-sign(number2) == -2:
        return -2
        # False negative

def addTimes(dataframe, mins):
    df = dataframe.index
    df = pandas.DataFrame(df,columns=['Fra_dato'])
    dataframe = dataframe.reset_index()
    df = df + pandas.Timedelta(minutes=mins)
    dataframe = pd.merge(dataframe,df, how='outer')
    #df = df.set_index(['Fra_dato'])
    dataframe = dataframe.set_index(['Fra_dato'])
    dataframe.drop(dataframe.tail(1).index,inplace=True)
    dataframe = dataframe.sort_index()
    return dataframe

def processGuesses(guesses, rowName):
    true_positives = guesses.loc[[1]].values[0]
    true_negatives = guesses.loc[[-1]].values[0]
    false_negatives = guesses.loc[[-2]].values[0]
    false_positives = guesses.loc[[2]].values[0]
    precision = true_positives / (true_positives + false_positives)
    precision_neg = true_negatives / (true_negatives + false_negatives)
    recall = true_positives / (false_negatives + true_positives)
    recall_neg = true_negatives / (false_positives + true_negatives)
    data = {'Precision' : [precision], 'Precision_neg' : [precision_neg], 'Recall' : [recall], 'Recall_neg' : [recall_neg]}
    df = pd.DataFrame(data, index=[rowName])
    #print(guesses)
    print(f'{rowName} TP: {true_positives} TN: {true_negatives} FP: {false_positives} FN: {false_negatives}')
    return df

def rule4(prevPower, currPower, prevIns, currIns):
    try:
        if (abs(prevPower)>abs(currPower) and prevIns > currIns) or (abs(prevPower)<abs(currPower) and prevIns<currIns):
            return -1
        else:
            return 1
    except:
        print('Lazy handling of start')
        return 1

def ensemble(dataframe,rulesList, weights):
    columns = []
    df_sign = pd.DataFrame()
    for i in range(len(rulesList)):
        columns.append('rule' + str(rulesList[i]) + '_pred')
        df_sign[columns[i]] = dataframe.apply(lambda x:sign(x[columns[i]])*weights[i], axis = 1)
    dataframe[f'{str(rulesList)}ensemble_pred'] = df_sign.loc[:, columns].sum(axis=1)
    return dataframe

if __name__ == '__main__':
    # getting usage data
    df_usage = prepData(filename='Meterdata_forbrug_20230329.ods')
    # Setting up offset for simulation
    end_validation = '2023-02-08 01:00:00 UTC'
    data_test = df_usage.loc[end_validation:, :]
    # Predictions, rule 1
    '''
    predictions = pd.read_csv('predictions.csv')
    predictions = predictions.rename(columns={'Unnamed: 0':'Fra_dato'})
    predictions = predictions.rename(columns={'pred':'rule1_pred'})
    predictions['Fra_dato'] = pd.to_datetime(predictions['Fra_dato'], format='%Y-%m-%d %H:%M:%S%z')
    predictions = predictions.set_index(['Fra_dato'])
    data_test = data_test.join(predictions)
    '''

    # Based on insolation mean, rule 2
    data_test['insolation'] = pd.to_numeric(data_test['insolation'])
    #print(f"mean is {data_test.loc[:,'insolation'].mean()}")
    #print(f"mean for negative power {data_test.loc[data_test['amount'] < 0, 'insolation'].mean()}")
    neg_mean = data_test.loc[data_test['amount'] < 0, 'insolation'].mean()
    data_test['rule2_pred'] = data_test['amount'].abs()*(neg_mean-data_test['insolation'])

    # Based on imperative insolation, rule 3
    #modified_neg_mean = neg_mean-neg_mean*0.6
    modified_neg_mean = 100
    data_test['rule3_pred'] = data_test['amount'].abs()*(modified_neg_mean-data_test['insolation'])

    # Based on change in usage and insolation, rule 4
    data_test['amount_prev'] = data_test['amount'].shift()
    data_test['insolation_prev'] = data_test['insolation'].shift()
    data_test['rule4_pred'] = data_test.apply(lambda x:rule4(x['amount_prev'],x['amount'],x['insolation_prev'],x['insolation']),axis = 1)
    data_test.drop('amount_prev', inplace = True, axis = 1)
    data_test.drop('insolation_prev', inplace = True, axis = 1)

    # Based on classifier, rule 5
    '''
    class_predictions = pd.read_csv('sign_predictions.csv', header=None)
    print(class_predictions.iloc[:,0])
    data_test['rule5_pred'] = class_predictions.iloc[:,0].values
    #print(data_test)
    '''

    '''
    # Ensemble of rule 1,3,4
    #data_test['rule5_pred'] = np.where(data_test['rule1_pred']<0 and (data_test['rule3_pred']<0 or data_test['rule4_pred']))
    rules = [1,3,5]
    weights = [1,1,1]
    data_test = ensemble(data_test,rules,weights)
    '''

    data_test['sign_comparison_1'] = data_test.apply(lambda x:isSignCorrect(x['rule1_pred'],x['amount']),axis = 1)
    data_test['sign_comparison_2'] = data_test.apply(lambda x:isSignCorrect(x['rule2_pred'],x['amount']),axis = 1)
    data_test['sign_comparison_3'] = data_test.apply(lambda x:isSignCorrect(x['rule3_pred'],x['amount']),axis = 1)
    data_test['sign_comparison_4'] = data_test.apply(lambda x:isSignCorrect(x['rule4_pred'],x['amount']),axis = 1)
    data_test['sign_comparison_5'] = data_test.apply(lambda x:isSignCorrect(x['rule5_pred'],x['amount']),axis = 1)
    data_test['sign_comparison_ensemble'] = data_test.apply(lambda x:isSignCorrect(x[f'{rules}ensemble_pred'],x['amount']),axis = 1)


    # Interpolating (stopped because of study integrity)
    #data_test = addTimes(data_test, 20)
    #data_test = addTimes(data_test, 40)
    #data_test['amount'] = data_test['amount'].interpolate(method = 'linear')
    guesses1 = data_test['sign_comparison_1'].value_counts()
    guesses2 = data_test['sign_comparison_2'].value_counts()
    guesses3 = data_test['sign_comparison_3'].value_counts()
    guesses4 = data_test['sign_comparison_4'].value_counts()
    guesses5 = data_test['sign_comparison_5'].value_counts()
    guessesEnsemble = data_test['sign_comparison_ensemble'].value_counts()
    guessEval1 = processGuesses(guesses1, rowName='pred1')
    guessEval2 = processGuesses(guesses2, rowName='pred2')
    guessEval3 = processGuesses(guesses3, rowName='pred3')
    guessEval4 = processGuesses(guesses4, rowName='pred4')
    guessEval5 = processGuesses(guesses5, rowName='pred5')
    guessEvalEnsemble = processGuesses(guessesEnsemble, rowName=f'{rules}_pred')
    guessEvalTotal = guessEval1
    guessEvalTotal = guessEvalTotal.append(guessEval2)
    guessEvalTotal = guessEvalTotal.append(guessEval3)
    guessEvalTotal = guessEvalTotal.append(guessEval4)
    guessEvalTotal = guessEvalTotal.append(guessEval5)
    guessEvalTotal = guessEvalTotal.append(guessEvalEnsemble)
    print(guessEvalTotal)
    #data_test['error'] = data_test['amount'] * data_test[f'{rules}ensemble_pred']
    #filtered_df = data_test.where(data_test['error']<0)
    #print(filtered_df[~filtered_df.isnull().any(axis=1)].to_string())