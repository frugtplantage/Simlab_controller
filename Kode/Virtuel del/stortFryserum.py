# -*- coding: utf-8 -*-
"""
Created on Thu Mar  9 11:31:33 2023

@author: Asmus
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from time import time
import requests
from datetime import datetime, timedelta
from dataclasses import dataclass
import parameters


@dataclass
class PIDController:
    Kp: float = 0
    Ki: float = 0
    Pterm: float = 0
    Iterm: float = 0
    previous_Iterm: float = 0
    templock: bool = False
    price_sensitivity = 0.1

    def update(self, setpoint, measurement):
        error = setpoint - measurement
        self.Pterm = error * self.Kp
        self.current_time = time()
        self.previous_time = time()
        self.Iterm = self.Ki * (self.previous_Iterm + error * (self.current_time - self.previous_time))
        self.previous_Iterm = self.Iterm
        u = self.Pterm + self.Iterm
        return u

    def setSetpoint(self, insolation, time, setpointtimes, setpoint):
        # Tjekker om der er sol på solcellerne, sker kun når der er data hver time
        if time % 2 == 0 and self.templock == False:
            if int(insolation.iloc[round(time / 12)][0][20:]) >= 300:
                setpoint = -18
            else:
                setpoint = -14

        if time / 12 in setpointtimes[1] and setpoint >= -20:
            setpoint = -18
            self.templock = True
        if i / 12 in setpointtimes[0]:
            setpoint = -10
        if i / 12 in setpointtimes[2]:
            setpoint = -14
            self.templock = False
        return setpoint

    def forecastSetpoint(self, currentprice, nextprice):
        if nextprice < currentprice - self.price_sensitivity:
            setpoint = -12
        elif nextprice > currentprice + self.price_sensitivity:
            setpoint = -18
        else:
            setpoint = -14

        return setpoint

    def calcCost(self, prices, ontime, power):
        cost = 0
        for i in range(23):
            price = prices[i]
            '''
            if int(insolation.iloc[i][0][20:]) >= 300:
                if i < 5:
                    price += -0.15090
                elif 16 <= i < 20:
                    price += -1.35840
                else:
                    price += -0.45280
            '''
            cost += np.sum(ontime[i * 3600:(i + 1) * 3600]) * price * power
        return cost


def getPrices():
    now = datetime.now()
    currdate = now.strftime("%Y-%m-%dT00:00")
    tomorrow = now + timedelta(days=1)
    stopdate = tomorrow.strftime("%Y-%m-%dT00:00")
    url = f'https://api.energidataservice.dk/dataset/Elspotprices?offset=0&start={currdate}&end={stopdate}&filter=%7B%22PriceArea%22:[%22DK2%22]%7D&sort=HourUTC%20DESC&timezone=dk'
    req = requests.get(url)
    prices = req.json()
    pricelist = []
    for i in range(len(prices['records'])):
        pricelist.append(prices['records'][i]['SpotPriceDKK'])
        # Tilføjer distribution
        if i < 5:
            pricelist[-1] += 0.15090 * 1000
        elif 16 <= i < 20:
            pricelist[-1] += 1.35840 * 1000
        else:
            pricelist[-1] += 0.45280 * 1000
    return pricelist


current_time = time()
previous_time = time()

setpoint = -14
measurementlist = [-14]
signal = 0
stoptimes = np.array([17])
preptimes = stoptimes - 2
starttimes = np.array([21])
stopprepstarttimes = np.array([[17], [15], [21]])
templock = False
# Genererer insolation
#insolation = pd.read_csv("Indstraaling.csv")
setpointlist = []
onofflist = np.array([])
# Forbrug i kW
energyUse = 2.25 / (60 * 60)

temp = getPrices()
temp.append(temp[-1])
pricelist = np.array(temp) / 1000

pid = PIDController(Kp=-0.8, Ki=0.3)
# Hvert 15 minut beregnes en ny tændingstid
for i in range(24 * 4):
    setpoint = pid.forecastSetpoint(pricelist[int(i / 4)], pricelist[int((i / 4) + 1)])
    signal = pid.update(setpoint, measurementlist[-1])
    # sikrer at kompressoren ikke bliver tændt i for kort tid, 0.33 = 5 minutter
    if signal < 0.33:
        signal = 0
    elif signal > 1:
        signal = 1
    for j in range(60 * 15):
        if j < 60 * 15 * signal:
            # Fryserum skal køle 3 grader pr 30 minutter ifølge gammel rapport
            measurementlist.append(measurementlist[-1] - 3 / (30 * 60))
            onofflist = np.append(onofflist, True)
        elif j >= 60 * 15 * signal:
            measurementlist.append(measurementlist[-1])
            onofflist = np.append(onofflist, False)
        # Skal stige 3 grader pr. 90 minut ifølge gammel rapport, 3 gange kølingstid
        # For at simulere højere tab ved lavere temperatur, antages udetemperatur er 10 grader.
        # Med gnms. temp -12.5 er tabet 3/(22.5*90)*temp per minut
        measurementlist[-1] += -3 / (22.5 * 90 * 60) * measurementlist[-1]
        setpointlist.append(setpoint)

cost = pid.calcCost(pricelist, onofflist[0:int(len(onofflist)*1)], energyUse)
print(measurementlist[int(len(measurementlist)*0.1)])
E_per_deg = -parameters.freezer_cool_speed/(parameters.freezer_energy_use/(60*60))
#cost = cost - (-14-measurementlist[-1])*E_per_deg*pricelist[-1]
print(f'cost: {cost}')

# Denne liste bruges til at plotte timebaseret data på det sekundbaserede plot
plotlist = []
for i in range(len(pricelist)):
    plotlist.append((i) * 60 * 60)

plotlist2 = []
for i in range(len(measurementlist)):
    plotlist2.append((i)/(60 * 60))

plotlist3 = []
for i in range(len(setpointlist)):
    plotlist3.append((i)/(60 * 60))

#plt.figure()
#plt.plot(measurementlist[0:18000])


fig, ax = plt.subplots()
plt.xticks(rotation = 45)
ax.plot(plotlist2,measurementlist, linewidth=0.5, label = 'Simulated Temperature')
ax.plot(plotlist3,setpointlist, label = 'Setpoint')
ax.plot(plotlist3,onofflist - 11, marker='.', linestyle='none', markersize=0.5, alpha=1, label = 'On/off')
plt.xlabel("Time[h]")
plt.ylabel("Temperature [C]")
plt.legend()
ax2=ax.twinx()
ax2.plot(pricelist, drawstyle='steps-post', label = 'Spot price', color = 'red')
ax2.set_ylabel("Spot Price [DKK/kWh]")
plt.legend()
plt.show()

