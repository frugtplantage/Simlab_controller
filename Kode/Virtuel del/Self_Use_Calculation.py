from util import prepData
from simpelPlot import read_Measurements
import numpy as np
import pandas as pd
import pytz
from datetime import timedelta
import parameters

simNr = 3
Freezer = False
EV = True
resolution_s = 90
resolution = str(resolution_s)+'s'
tics_per_hour = 3600/resolution_s
#df_usage = prepData(filename='Meterdata_forbrug_20230329.ods')
#print(len(df_usage['amount']))
#print((df_usage['amount'].between(-1000000,0)).value_counts())
#print(1710/8712)
#print(1710/2281)

meas_data = read_Measurements(simNr=simNr,directory='data/measurements_saved_2')
meas_data_reference = read_Measurements(simNr=3,directory='data/measurements_saved_2')
for i in range(len(meas_data)):
    meas_data[i].index = meas_data[i].index.tz_localize(pytz.utc)
    meas_data_reference[i].index = meas_data_reference[i].index.tz_localize(pytz.utc)
    #meas_data[i].index = meas_data[i].index-timedelta(days=71)
    #meas_data_reference[i].index = meas_data_reference[i].index-timedelta(days=71)
tempDF = meas_data[0]
setpointDF = meas_data[1]
priceDF = meas_data[2]
usageDF = meas_data[3]
socDF = meas_data[4]

ref_tempDF = meas_data_reference[0]
ref_usageDF = meas_data_reference[3]

usageDF = usageDF.resample(resolution).mean()
usageDF = usageDF.fillna(0)
ref_usageDF = ref_usageDF.resample(resolution).mean()
ref_usageDF = ref_usageDF.fillna(0)
#usageDF['only balance'] = usageDF['usage'] - usageDF['freezer usage'] - usageDF['EV usage']
usageDF['only balance'] = ref_usageDF['usage'] - ref_usageDF['freezer usage'] - ref_usageDF['EV usage']
usageDF['only balance'] = usageDF['only balance']/1

if Freezer:
    E_loss = (sum(usageDF['freezer usage'])-sum(ref_usageDF['freezer usage']))/tics_per_hour
else:
    E_loss = 0
# Calculating change in energy for freezer
if Freezer:
    E_per_deg = -parameters.freezer_cool_speed/(parameters.freezer_energy_use/(60*60))
    delta_E_ref = (-14-ref_tempDF.tail(1).values[0][0])*E_per_deg
    delta_E_meas = (-14-tempDF.tail(1).values[0][0])*E_per_deg
    print(ref_tempDF.tail(1).values[0][0])
    print(tempDF.tail(1).values[0][0])
    plt.plot(tempDF)
    plt.show()
    delta_E_ESS = delta_E_meas-delta_E_ref
    print(delta_E_ESS)
else:
    delta_E_ESS = 0

E_gen_index = np.where(usageDF['only balance'] < 0)
E_gen = -sum(usageDF['only balance'].iloc[E_gen_index])/tics_per_hour
# Import/export looking only at freezer
# Removing all energy use that isn't freezer
usageDF['exp only'] = usageDF['only balance']
exp_only_index = np.where(usageDF['only balance']>0)
usageDF['exp only'].iloc[exp_only_index] = 0

usageDF['units'] = usageDF['exp only']
if Freezer:
    usageDF['units'] += usageDF['freezer usage']
if EV:
    usageDF['units'] += usageDF['EV usage']

E_imp_index = np.where(usageDF['units'] > 0)
E_imp = sum(usageDF['units'].iloc[E_imp_index])/tics_per_hour
kappa = E_gen/(E_imp+E_gen)

E_load = 0
if Freezer:
    E_load += sum(usageDF['freezer usage'])/tics_per_hour
if EV:
    E_load += sum(usageDF['EV usage'])/tics_per_hour


E_exp_index = np.where(usageDF['units'] < 0)
try:
    E_exp = -sum(usageDF['units'].iloc[E_exp_index])/tics_per_hour
except:
    E_exp = 0


sigma_1 = (E_gen - kappa * (E_exp +E_loss + delta_E_ESS))/E_load
sigma_2 = (E_gen - E_exp - kappa * (E_loss + delta_E_ESS))/E_load
sigma_3 = (E_gen - E_exp - E_loss - kappa * delta_E_ESS)/E_load

print(f'E_gen: {E_gen}')
print(f'kappa: {kappa}')
print(f'(E_exp + E_loss + delta_E_ESS): {(E_exp + E_loss + delta_E_ESS)}')
print(f'E_loss: {E_loss}')
print(f'E_loss_percent: {E_loss/E_load}')
print(f'delta_E_ESS: {delta_E_ESS}')
print(f'E_exp: {E_exp}')
print(f'E_load: {E_load}')
print(f'E_import: {E_imp}')
print(f'sigma_1:{sigma_1}')
print(f'sigma_2:{sigma_2}')
print(f'sigma_3:{sigma_3}')

export_usage_freezer_list = []
for i,row in usageDF.iterrows():
    export_usage_freezer_list.append(max(min(row['only balance']/(-row['freezer usage']+0.00001),1),0))
usageDF['export_usage_freezer'] = export_usage_freezer_list

export_usage_EV_list = []
for i,row in usageDF.iterrows():
    export_usage_EV_list.append(max(min(row['only balance']/(-row['EV usage']+0.00001),1),0))
usageDF['export_usage_EV'] = export_usage_EV_list

usageDF=usageDF.fillna(0)
#print(sum([usageDF<0][0]['only balance']*usageDF['freezer usage'])/sum(usageDF['freezer usage']))
surplus_usage_freezer = sum(usageDF['export_usage_freezer']*usageDF['freezer usage'])
surplus_usage_EV = sum(usageDF['export_usage_EV']*usageDF['EV usage'])
print(f"Udnyttelse af overskudseffekt fryser: {surplus_usage_freezer/sum(usageDF['freezer usage'])}")
print(f"Udnyttelse af overskudseffekt EV: {surplus_usage_EV/sum(usageDF['EV usage'])}")
print(f"Udnyttelse af overskudseffekt sammen: {(surplus_usage_EV+surplus_usage_freezer)/(E_load*tics_per_hour)}")
print((sum(usageDF['freezer usage'])-sum(ref_usageDF['freezer usage']))/tics_per_hour)

