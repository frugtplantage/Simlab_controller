from time import time, sleep
import zmq
import sys
import random
from json import dump
from datetime import datetime
import parameters
import paho.mqtt.client as mqtt

message_received = 0


def on_message(client, userdata, message):
    # print("received message: " ,str(message.payload.decode("utf-8")))
    global message_received
    sleep(0.1)
    message_received = float(message.payload.decode("utf-8"))
    # print(message_received)


# Initialisering
coolSpeed = parameters.freezer_cool_speed
temp = -13
# Skal stige 3 grader pr. 120 minut ved -12 grader ifølge gammel rapport, 3 gange kølingstid
# For at simulere højere tab ved lavere temperatur, antages udetemperatur er 10 grader
# R = (Tud-Tind)/deltaT
# R = 52800 sekunder
# deltaT = (Tud-Tind)/R
# K/s = (10-Tind)/43200
cool = 0

# # Communication
'''
mqttBroker ="mqtt.eclipseprojects.io"
client = mqtt.Client("Fryser")
client.connect(mqttBroker)
client.loop_start()
client.subscribe("OP")
client.on_message=on_message
'''

# region ZMQ
context = zmq.Context()
temp_out_socket = context.socket(zmq.PUB)
portout = parameters.freezer_port_3
temp_out_socket.bind(f"tcp://*:{portout}")

op_in_socket = context.socket(zmq.SUB)
portin = parameters.freezer_controller_port_3
op_in_socket.connect(f"tcp://{'localhost'}:{portin}")
op_in_socket.subscribe(parameters.freezer1_op_topic_3)
# endregion

LOG_FILE = f'data/measurements/measurements_simulator_3_{time():.00f}.json'
print(f"Logging to file {LOG_FILE}")
runTime = 20 * 60 * 60

try:
    while True:
        # To ensure loop runs once per second
        startTime = time()
        try:
            cool = float(op_in_socket.recv_string(flags=zmq.NOBLOCK).split(';')[-1])
            print(f'Received message: {cool}')
            # cool = message_received
        except:
            pass
        if cool == 1:
            temp += coolSpeed
        else:
            temp += (10 - temp) / 52800
        # client.publish(f"{parameters.freezer1_topic}", temp)
        temp_out_socket.send_string(f"{parameters.freezer1_topic_3};{str(datetime.now())};{str(temp)}")
        print(f"Published topic {parameters.freezer1_topic_3}: {temp}")
        temp_out_socket.send_string(f"{parameters.freezer1_op_out_topic_3};{str(cool * parameters.freezer_energy_use)}")
        print(f"Published topic {parameters.freezer1_op_out_topic_3}: {cool * parameters.freezer_energy_use}")

        # Writing data to log
        with open(LOG_FILE, 'a') as file:
            dump([temp, cool, datetime.now()], file, default=str)
            file.write('\n')
        sleep(max(1 - (time() - startTime), 0))
except KeyboardInterrupt:
    # client.loop_stop()
    pass

