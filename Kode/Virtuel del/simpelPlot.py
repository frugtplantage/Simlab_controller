import pandas
import pandas as pd
import json
import matplotlib.pyplot as plt
import os
import numpy as np
from datetime import timedelta, date

def Extract(lst, index = 0):
    #lst=lst[0::100]
    return [item[index] for item in lst]

def read_Measurements(simNr, directory = 'data/measurements'):
    # region Reading files
    DATA_MEAS_DIR = directory
    # Always plot latest datafile - replace [-1] with another index if you want to plot a specific file.
    MEAS_LOG_FILE1 = sorted(os.listdir(DATA_MEAS_DIR))[12 + simNr]
    print(f"Using {MEAS_LOG_FILE1} as freezer temp data")
    MEAS_LOG_FILE2 = sorted(os.listdir(DATA_MEAS_DIR))[3 + simNr]
    print(f"Using {MEAS_LOG_FILE2} as freezer setpoint data")
    MEAS_LOG_FILE3 = sorted(os.listdir(DATA_MEAS_DIR))[15 + simNr]
    print(f"Using {MEAS_LOG_FILE3} as price data")
    MEAS_LOG_FILE4 = sorted(os.listdir(DATA_MEAS_DIR))[9 + simNr]
    print(f"Using {MEAS_LOG_FILE4} as usage data")
    MEAS_LOG_FILE5 = sorted(os.listdir(DATA_MEAS_DIR))[0 + simNr]
    print(f"Using {MEAS_LOG_FILE5} as EV charger data")

    # Open the file, process each json message into a python dictionary

    with open(os.path.join(DATA_MEAS_DIR, MEAS_LOG_FILE1)) as f:
        temp_meas_data = [json.loads(line) for line in f]

    with open(os.path.join(DATA_MEAS_DIR, MEAS_LOG_FILE2)) as f:
        setpoint_meas_data = [json.loads(line) for line in f]

    pricedf = pandas.read_csv(os.path.join(DATA_MEAS_DIR, MEAS_LOG_FILE3))

    with open(os.path.join(DATA_MEAS_DIR, MEAS_LOG_FILE4)) as f:
        usage_meas_data = [json.loads(line) for line in f]

    with open(os.path.join(DATA_MEAS_DIR, MEAS_LOG_FILE5)) as f:
        charge_meas_data = [json.loads(line) for line in f]
    # endregion

    # region Prepping Freezer Data
    tempList = Extract(temp_meas_data)
    tempdateList = Extract(temp_meas_data, index=2)
    tempDF = pandas.DataFrame({'temp': tempList, 'date': tempdateList})
    tempDF['date'] = pd.to_datetime(tempDF['date'], format='%Y-%m-%d %H:%M:%S.%f')
    tempDF['temp'] = tempDF['temp'].astype(float)
    tempDF = tempDF.set_index('date')

    setpointList = Extract(setpoint_meas_data, index=0)
    setpointDateList = Extract(setpoint_meas_data, index=3)
    setpointDF = pandas.DataFrame({'setpoint': setpointList, 'date': setpointDateList})
    setpointDF['date'] = pd.to_datetime(setpointDF['date'], format='%Y-%m-%d %H:%M:%S.%f')
    setpointDF['setpoint'] = setpointDF['setpoint'].astype(float)
    setpointDF = setpointDF.set_index('date')

    # endregion

    # region Prepping Common Data
    pricedf['HourDK'] = pd.to_datetime(pricedf['HourDK'], format='%Y-%m-%d %H:%M:%S.%f')
    pricedf['SpotPriceDKK'] = pricedf['SpotPriceDKK'].astype(float)
    pricedf = pricedf.set_index('HourDK')
    #pricedf = pricedf.drop('HourUTC', axis=1)
    pricedf = pricedf.drop_duplicates()
    pricedf = pricedf.sort_index()

    usageList = Extract(usage_meas_data, index=3)
    fUsageList = Extract(usage_meas_data,index=0)
    EVUsageList = Extract(usage_meas_data,index=1)
    usageDateList = Extract(usage_meas_data, index=2)
    usageDF = pandas.DataFrame({'usage': usageList, 'freezer usage' : fUsageList, 'EV usage' : EVUsageList, 'date': usageDateList})
    usageDF['date'] = pd.to_datetime(usageDF['date'], format='%Y-%m-%d %H:%M:%S.%f')
    usageDF['usage'] = usageDF['usage'].astype(float)
    usageDF = usageDF.set_index('date')
    # endregion

    # region Prepping EV Data
    socList = Extract(charge_meas_data)
    socdateList = Extract(charge_meas_data, index=2)
    socDF = pandas.DataFrame({'soc': socList, 'date': socdateList})
    socDF['date'] = pd.to_datetime(socDF['date'], format='%Y-%m-%d %H:%M:%S.%f')
    socDF['soc'] = socDF['soc'].astype(float)
    socDF = socDF.set_index('date')
    # endregion
    return [tempDF,setpointDF,pricedf,usageDF,socDF]


if __name__ == '__main__':
    simNr = 3
    data = read_Measurements(simNr=simNr, directory='data/measurements_saved_2')
    tempDF = data[0]
    setpointDF = data[1]
    pricedf = data[2]
    usageDF = data[3]
    socDF = data[4]
    #df = pd.concat([tempDF,setpointDF,pricedf])
    #df = df.sort_index()
    #df = df.resample('10s').mean()
    #plt.plot(tempdateList,tempList)
    #plt.plot(setpointDateList, setpointList)
    #plt.plot(setpointList)
    fig, ax = plt.subplots()

    ax.set_xlim([tempDF.head(1).index[0], tempDF.tail(1).index[0]+timedelta(hours=1)])
    plt.xticks(rotation = 45)
    lns1 = ax.plot(tempDF, label = 'Temperature')
    lns2 = ax.plot(setpointDF,drawstyle = 'steps-post' , label = 'Setpoint')
    ax.set_ylabel("Temperature [C]")
    #ax.plot(usageDF, label = 'usage')


    if simNr == 1:
        ax2 = ax.twinx()
        lns3 = ax2.plot(pricedf,drawstyle='steps-post', color = 'green', label = 'Spot price', linestyle='dashed')
        ax2.set_ylabel("Spot Price [DKK/kWh]")
    elif simNr == 2:
        ax2 = ax.twinx()
        lns3 = ax2.plot(usageDF['usage'],drawstyle='steps-post', color = 'red', label = 'Power Balance')
        ax2.set_ylabel("Power [kW]")
    elif simNr == 3:
        ax2 = ax.twinx()
        lns3 = ax2.plot(socDF, label = 'SOC', color='red')
        ax2.set_ylabel("SOC")
    try:
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc="lower right")
    except:
        pass
    plt.show()

    fig, ax = plt.subplots()
    ax.set_xlim([tempDF.head(1).index[0], tempDF.tail(1).index[0] + timedelta(hours=1)])
    plt.xticks(rotation=45)
    ax.plot(pricedf, drawstyle='steps-post', color='red', label='Spot price')
    ax.set_ylabel("SpotPrice [DKK]")
    ax.legend()
    plt.show()


    fig, ax = plt.subplots()
    ax.set_xlim([tempDF.head(1).index[0], tempDF.tail(1).index[0] + timedelta(hours=1)])
    plt.xticks(rotation=45)
    ax.plot(usageDF['usage'],drawstyle='steps-post', color = 'red', label = 'Power Balance')
    ax.set_ylabel("Power [kW]")
    ax.legend()
    plt.show()
    lns2 = False

    fig, ax = plt.subplots()
    ax.set_xlim([tempDF.head(1).index[0], tempDF.tail(1).index[0] + timedelta(hours=1)])
    plt.xticks(rotation=45)
    lns1 = ax.plot(socDF, label = 'SOC')
    ax.set_ylabel("SOC")
    ax2 = ax.twinx()
    if simNr == 1:
        lns2 = ax2.plot(pricedf, drawstyle='steps-post', color = 'green', label = 'Spot price', linestyle='dashed')
        ax2.set_ylabel("SpotPrice [DKK/kWh]")
    elif simNr == 2:
        lns2 = ax2.plot(usageDF['usage'],drawstyle='steps-post', color = 'red', label = 'Power Balance')
        ax2.set_ylabel("Power [kW]")
    try:
        lns = lns1 + lns2
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc=0)
    except:
        ax.legend()
    plt.show()

    fig, ax = plt.subplots()
    ax.set_xlim([tempDF.head(1).index[0], tempDF.tail(1).index[0] + timedelta(hours=1)])
    plt.xticks(rotation=45)
    ax.plot(usageDF['usage'],drawstyle='steps-post', color = 'red', label = 'Power Balance')
    ax.legend()
    ax.set_ylabel("Power [kW]")
    plt.show()


