# -*- coding: utf-8 -*-
"""
Created on Thu Mar  9 11:31:33 2023

@author: Asmus
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from time import time, sleep

import pytz
from datetime import datetime, timedelta
from dataclasses import dataclass
import zmq
from json import dump
from util import getPrices, priceAt
import parameters
import paho.mqtt.client as mqtt


@dataclass
class Fryserum:
    Kp: float = 0
    Ki: float = 0
    Pterm: float = 0
    Iterm: float = 0
    previous_Iterm: float = 0
    templock: bool = False
    price_sensitivity: float = 0
    minTemp: float = 0
    baseTemp: float = 0
    maxTemp: float = 0
    power: float = 0
    # How much power needs to be leftover after turning on to turn on cooling
    powerReserve: float = 0

    def update(self, setpoint, measurement):
        error = setpoint - measurement
        self.Pterm = error * self.Kp
        self.current_time = time()
        self.previous_time = time()
        self.Iterm = self.Ki * (self.previous_Iterm + error * (self.current_time - self.previous_time))
        self.previous_Iterm = self.Iterm
        u = self.Pterm + self.Iterm
        return u

    def forecastSetpoint(self, currentprice, nextprice):
        if nextprice < currentprice - self.price_sensitivity:
            setpoint = self.maxTemp
        elif nextprice > currentprice + self.price_sensitivity:
            setpoint = self.minTemp
        else:
            setpoint = self.baseTemp

        return setpoint

    # Cools heavily if there is leftover power, stops cooling if there will soon be leftover power
    def selfUseSetpoint(self, netPower, nextNetPower, cool):
        # Checking if there is leftover power, or if the system is already cooling and netPower is positive
        if netPower <= -self.power or (netPower <= -self.powerReserve and cool == 1):
            setpoint = self.minTemp
        elif nextNetPower < -self.power:
            setpoint = self.maxTemp
        else:
            setpoint = self.baseTemp
        return setpoint

    def fallbackSetpoint(self):
        # If no current prices are received, reverts to basic pattern based around nettariffs.
        if 4 <= datetime.now().hour < 6 or 15 <= datetime.now().hour < 17:
            setpoint = self.minTemp
        elif 17 <= datetime.now().hour < 21:
            setpoint = self.maxTemp
        else:
            setpoint = self.baseTemp
        return setpoint


message_received = ''
nextPow = 0
pow = 0
pow_time = datetime.now()
head_permission = 0
head_permission_time = datetime.now()


def on_message(client, userdata, message):
    client.publish(f"{parameters.freezer_heartbeat_2}", cool)
    if message.topic == parameters.freezer1_topic_2:
        global message_received
        message_received = float(message.payload.decode("utf-8"))
    if message.topic == parameters.head_port_forecast_topic_2:
        global nextPow
        nextPow = float(message.payload.decode("utf-8"))
    if message.topic == parameters.head_port_usage_topic_2:
        global pow, pow_time
        pow = float(message.payload.decode("utf-8"))
        pow_time = datetime.now()
    if message.topic == parameters.freezer_pow_answer_2:
        global head_permission
        global head_permission_time
        head_permission = float(message.payload.decode("utf-8"))
        head_permission_time = datetime.now()
    sleep(0.3)

def update_prices(pricedf):
    tempPrice = getPrices()
    tempPrice = tempPrice.set_index('HourDK')
    pricedf = pricedf.append(tempPrice)
    pricedf = pricedf[~pricedf.index.duplicated(keep='first')]
    return pricedf

# region Unit dependent parameters
min_ontime = 5 * 60
offtime_cutoff = 20 * 60
resolution = 20 * 60
setpoint = -14
currentTemp = -14
# Usage in kW
energyUse = parameters.freezer_energy_use
signal = 0
minTemp = -20
baseTemp = -14
maxTemp = -10
price_sensitivity = 0.15
# endregion

# Program parameters
pricePulled = False
forecasted = False
temp_Timeout = 0
usage_Timeout = 0
forecast_Timeout = 0
price_Timeout = 0
exog = False
cool = 0
# Choosing whether to optimize for price or self use
priceOpt = False
selfUseOpt = True

while True:
    if (datetime.now() - pow_time).seconds >= 60:
        pow = 0
        print('Has not received signal from head control for 60 seconds, setting power to 0')
    try:
        pricedf = getPrices(init=True)
        pricedf = pricedf.set_index('HourDK')
        print(f'Got the initial prices!')

    except:
        print("Can't access prices! Trying again")
        sleep(1)
        if price_Timeout >= 10:
            "Can't access prices, starting in fallback mode"
            break
        price_Timeout += 1
        continue
    break

try:
    pricedf = update_prices(pricedf)
except:
    pass

# region Communication
mqttBroker = "mqtt.eclipseprojects.io"
try:
    client = mqtt.Client("freezer_Sim_2")
    client.connect(mqttBroker)
    client.subscribe(parameters.head_port_usage_topic_2)
    client.subscribe(parameters.freezer1_topic_2)
    client.subscribe(parameters.head_port_forecast_topic_2)
    client.subscribe(parameters.freezer_pow_answer_2)
    client.loop_start()
    client.on_message = on_message
except:
    print('Can not access MQTT Broker')
# topic = 'OP'

# endregion

# region ZMQ (not used)
context = zmq.Context()
temp_in_socket = context.socket(zmq.SUB)
portin = parameters.freezer_port_2
temp_in_socket.setsockopt(zmq.CONFLATE, 1)
temp_in_socket.connect(f"tcp://{'localhost'}:{portin}")
temp_in_socket.subscribe(parameters.freezer1_topic_2)

op_out_socket = context.socket(zmq.PUB)
portout = parameters.freezer_controller_port_2
op_out_socket.bind(f"tcp://*:{portout}")

'''
head_usage_in_socket = context.socket(zmq.SUB)
head_portin = parameters.head_port
head_usage_in_socket.connect(f"tcp://{'localhost'}:{head_portin}")
head_usage_in_socket.subscribe(parameters.head_port_usage_topic)

head_forecast_in_socket = context.socket(zmq.SUB)
head_forecast_in_socket.connect(f"tcp://{'localhost'}:{head_portin}")
head_forecast_in_socket.subscribe(parameters.head_port_forecast_topic)
'''
# endregion


pid = Fryserum(Kp=-0.8, Ki=0.3, minTemp=minTemp, baseTemp=baseTemp, maxTemp=maxTemp,
               price_sensitivity=price_sensitivity, power=energyUse)

# region Logging
LOG_FILE = f'data/measurements/measurements_controller_2_{time():.00f}.json'
print(f"Logging to file {LOG_FILE}")

PRICELOG_FILE = f'data/measurements/prices_{time():.00f}.csv'
print(f"Logging prices to file {PRICELOG_FILE}")
# endregion
sleep(2)

try:
    while True:
        # Her måles temperatur
        while True:
            try:
                currentTemp = float(temp_in_socket.recv_string(flags=zmq.NOBLOCK).split(';')[-1])
                print(f"Received temperature: {currentTemp}")
                temp_Timeout = 0
            except:
                print("Can't access current temperature! Trying again in a second")
                temp_Timeout += 1
                if temp_Timeout >= 10:
                    currentTemp = 20
                    print("Still can't access temperature, setting safety temperature of 20")
                    break
                sleep(1)
                continue
            break

        # Pulling new prices
        if 21 < datetime.now().hour < 24 and pricePulled == False:
            try:
                pricedf = update_prices(pricedf)
                pricePulled = True
            except:
                print("Can't access prices! Reverting to fallback control")

        if datetime.now().hour <= 21 and pricePulled == True:
            pricePulled = False
        if priceOpt:
            try:
                setpoint = pid.forecastSetpoint(priceAt(pricedf123, netPower=pow),
                                                priceAt(pricedf, hourDelta=1, netPower=nextPow))
            except:
                try:
                    # Trying to access prices again
                    try:
                        pricedf = update_prices(pricedf)
                    except:
                        pricedf = getPrices(init=True)
                        pricedf = pricedf.set_index('HourDK')
                        pricedf = update_prices(pricedf)
                    pricePulled = True
                    setpoint = pid.forecastSetpoint(priceAt(pricedf, netPower=pow),
                                                    priceAt(pricedf, hourDelta=1, netPower=nextPow))
                    print('Got the prices!')
                except Exception as e:
                    print(e)
                    print('Using fallback Setpoint')
                    setpoint = pid.fallbackSetpoint()

        elif selfUseOpt:
            while True:
                if isinstance(pow, float) and isinstance(nextPow, float):
                    print(f'current power: {pow}, next power: {nextPow}, setpoint: {setpoint}')
                    break
                elif isinstance(pow, float):
                    usage_Timeout = 0
                    forecast_Timeout += 1
                    if forecast_Timeout >= 10:
                        nextPow = 0
                        print('Cannot access forecast data, setting 0')
                        break
                    sleep(1)
                    continue
                elif isinstance(nextPow, float):
                    forecast_Timeout = 0
                    usage_Timeout += 1
                    if usage_Timeout >= 10:
                        pow = 0
                        print('Cannot access usage data, setting 0')
                        break
                    sleep(1)
                    continue
                else:
                    print(
                        f"Missed forecast {forecast_Timeout} times and missed usage {usage_Timeout} times. Trying again.")
                    usage_Timeout += 1
                    forecast_Timeout += 1
                    print(f'pow {pow}, nextpow {nextPow}, temp {message_received}')
                    if usage_Timeout >= 10 or forecast_Timeout >= 10:
                        pow = 0
                        nextPow = 0
                        print("Still can't access usage and forecast data, setting to 0")
                        break
                    sleep(1)
                    continue
            setpoint = pid.selfUseSetpoint(netPower=pow, nextNetPower=nextPow, cool=cool)
        else:
            setpoint = -14

        # Benytter PID funktionalitet. Fordel: mindre undershoot, hage: indeler i diskrete dele og har langsommere reaktion
        signal = pid.update(setpoint, currentTemp)
        print(f"Current signal is: {signal} and current setpoint is: {setpoint}, current power: {pow}")
        if signal > 0 and selfUseOpt and pow < 0:
            # Requesting to use the surplus power
            client.publish(f"{parameters.freezer_pow_request_2}", parameters.freezer_energy_use)

        # If permission is not received or permission is too old, don't cool. Overwritten if no surplus or too high temp
        if selfUseOpt and pow < 0 and currentTemp < maxTemp and (
                not head_permission or (datetime.now() - head_permission_time).seconds > 60):
            print('Permission to use power not received')
            signal = 0
        else:
            pass
            #print(head_permission)
            #print(head_permission_time)
        if signal > 1:
            signal = 1
        elif signal > 0:
            # Begrænser on/off blokkes størrelse til offtime_cutoff
            if (min_ontime / signal - min_ontime) > offtime_cutoff:
                signal = 0

        # Writing data to log

        with open(LOG_FILE, 'a') as file:
            dump([setpoint, currentTemp, signal, datetime.now(), pow, nextPow], file, default=str)
            file.write('\n')

        if signal > 0:
            cool = 1
            # client.publish(f"{topic}",cool)
            op_out_socket.send_string(f"{parameters.freezer1_op_topic_2};{cool}")
            print(f"Published topic {parameters.freezer1_op_topic_2}: {cool}")
            sleep(min_ontime)
            if signal < 1:
                cool = 0
                # client.publish(f"{topic}",cool)
                op_out_socket.send_string(f"{parameters.freezer1_op_topic_2};{cool}")
                print(f"Published topic {parameters.freezer1_op_topic_2}: {cool}")
                sleep(min_ontime / signal - min_ontime)
        else:
            cool = 0
            # client.publish(f"{topic}",cool)
            op_out_socket.send_string(f"{parameters.freezer1_op_topic_2};{cool}")
            print(f"Published topic {parameters.freezer1_op_topic_2}: {cool}")
            sleep(1)

        # Ren if baseret kontrol forslag
        '''
        if currentTemp > setpoint+1:
            cool = 1
            op_out_socket.send_string(f"{topic};{cool}")
            print(f"Published topic {topic}: {cool}")
        elif time()-starttime >= min_ontime and currentTemp <= setpoint:
            cool = 0
            op_out_socket.send_string(f"{topic};{cool}")
            print(f"Published topic {topic}: {cool}")
            starttime = time()

        sleep(1)
        '''

except KeyboardInterrupt:
    client.loop_stop()
    pricedf.to_csv(PRICELOG_FILE)
    # with open(PRICELOG_FILE, 'a') as file:
    # dump(pricedf, file, default=str)
