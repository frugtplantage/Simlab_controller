import os
import io
import json
import pandas as pd
import parameters
from simpelPlot import read_Measurements
from util import getTariff, getPrices

power = parameters.freezer_energy_use
simNr = 3
resolution_s = 90
resolution = str(resolution_s)+'s'
tics_per_hour = 3600/resolution_s
data = read_Measurements(simNr=simNr, directory='data/measurements_saved_2')
tempDF = data[0]
setpointDF = data[1]
pricedf = data[2]
usageDF = data[3]
socDF = data[4]

pricedf = pricedf.resample(resolution).last()
pricedf = pricedf.fillna(method = 'ffill')
usageDF = usageDF.resample(resolution).mean()
usageDF = usageDF.join(pricedf)
# Reducing price if self-use

#print(usageDF)
tariffList = []
for i,row in usageDF.iterrows():
    if row['usage'] < row['freezer usage'] + row['EV usage']:
        import_relative = max(max(row['usage'],0)/(row['freezer usage']+row['EV usage']+0.0000001),1)
        export_relative = 1-import_relative
        tariff = -(getTariff(i.hour)+0.05)*export_relative
    else:
        tariff = 0
    tariffList.append(tariff)
usageDF['SpotPriceDKK'] = usageDF['SpotPriceDKK']-tariffList

#print(usageDF)
usageDF['freezer cost'] = usageDF['freezer usage']*usageDF['SpotPriceDKK']/tics_per_hour
usageDF['EV cost'] = usageDF['EV usage']*usageDF['SpotPriceDKK']/tics_per_hour
#print(usageDF.to_string())
E_per_deg = -parameters.freezer_cool_speed/(parameters.freezer_energy_use/(60*60))

for i,row in usageDF.iterrows():
    if row['freezer usage'] > 0:
        last_price = row['SpotPriceDKK']

freezerCost = sum(usageDF['freezer cost'])
EVCost = sum(usageDF['EV cost'])
print(freezerCost)
print(freezerCost-E_per_deg*last_price*(tempDF.tail(1)['temp'][0]-(-14)))
print(EVCost)



'''
DATA_MEAS_DIR = 'data/measurements'
# Use price file
MEAS_LOG_FILE1 = sorted(os.listdir(DATA_MEAS_DIR))[-1]
print(f"Using {MEAS_LOG_FILE1} as price data")
# Use simulator file
MEAS_LOG_FILE2 = sorted(os.listdir(DATA_MEAS_DIR))[8]
print(f"Using {MEAS_LOG_FILE2} as consumption data")

with open(os.path.join(DATA_MEAS_DIR, MEAS_LOG_FILE2)) as f:
    consumption_data = [json.loads(line) for line in f]
df_price = pd.read_csv(os.path.join(DATA_MEAS_DIR, MEAS_LOG_FILE1),  skip_blank_lines=False)
df_consumption = pd.DataFrame(consumption_data, columns=['temperature','usage' ,'HourUTC'])
df_consumption['HourUTC'] = pd.to_datetime(df_consumption['HourUTC'], format = '%Y-%m-%d %H:%M:%S.%f')
df_consumption['HourUTC'] = df_consumption['HourUTC'].round('H')
df_consumption=df_consumption.drop('temperature', axis = 1)
df_consumption = df_consumption.groupby('HourUTC').sum()
df_price['HourUTC'] = pd.to_datetime(df_price['HourUTC'], format = '%Y-%m-%d %H:%M:%S.%f')
# Multiplying seconds on with power to get it in kWh
df_consumption['usage'] = df_consumption['usage']*power
df_price = df_price.set_index('HourUTC')
print(df_price.index)
print(df_consumption.index)

df = df_consumption.join(df_price)
df['cost']=df['usage']*df['SpotPriceDKK']
print(df)

cost = df['cost'].sum()
print(cost)
#print(test[0][0])
'''