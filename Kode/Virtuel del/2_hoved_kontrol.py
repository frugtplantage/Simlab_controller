
from datetime import datetime, timedelta
import pandas as pd
import pytz
from dateutil import parser
from time import time, sleep
from util import prepData, getMetObsIns, getForecastIns, getIns, addDateInfo
import parameters
import paho.mqtt.client as mqtt
from joblib import load
import random
import zmq
from json import dump
import numpy as np

def addMissingDummies(df):
    for i in range(12):
        if f'month_{i+1}' not in df:
            df[f'month_{i+1}'] = 0
    for i in range(24):
        if f'hour_{i}' not in df:
            df[f'hour_{i}'] = 0
    for i in range(7):
        if f'weekday_{i}' not in df:
            df[f'weekday_{i}'] = 0
    return df

        # Programparametre

freezer_status = 0
ev_status = 0
freezer_power = 0
ev_power = 0
freezer_status_time = datetime.now()
ev_status_time = datetime.now()
usage_timeout = 0
usage = 0
pow = 0
def on_message(client, userdata, message):
    #print("received message: " ,str(message.payload.decode("utf-8")))
    #print('Topic was', message.topic)
    global freezer_status_time, ev_status_time, freezer_status, ev_status, freezer_power, ev_power,pow
    # If heartbeat wasn't received for 60 seconds, set unit status as off
    if (datetime.now()-ev_status_time).seconds <= 60:
        ev_status = 0
    if (datetime.now()-freezer_status_time).seconds <= 60:
        freezer_status = 0
    if message.topic == parameters.freezer_heartbeat_2:
        # Receiving 0 if not cooling and 1 if cooling
        freezer_status = int(message.payload.decode("utf-8"))
        freezer_status_time = datetime.now()
    if message.topic == parameters.ev_heartbeat_2:
        ev_status = int(message.payload.decode("utf-8"))
        ev_status_time = datetime.now()
    if message.topic == parameters.freezer_pow_request_2:
        print('Received request from freezer')
        freezer_power = float(message.payload.decode("utf-8"))
        # Freezer gets permission if there isn't enough power for the EV, there is enough for them both, or EV is off
        #if not (-ev_power > pow > -(ev_power+freezer_power) and ev_status):
        if pow <= -freezer_power or (freezer_status and pow <= 0):
            client.publish(f"{parameters.freezer_pow_answer_2}", 1)
        else:
            client.publish(f"{parameters.freezer_pow_answer_2}", 0)
    if message.topic == parameters.ev_pow_request_2:
        ev_power = float(message.payload.decode("utf-8"))
        # EV charges if there is enough power
        if pow <= -ev_power or (ev_status and pow <= 0):
            client.publish(f"{parameters.ev_pow_answer_2}", 1)
        elif (pow-freezer_power)  <= -ev_power and freezer_status == 1:
            # Turn off freezer if it would allow EV to charge
            client.publish(f"{parameters.freezer_pow_answer_2}", 0)
        else:
            client.publish(f"{parameters.ev_pow_answer_2}", 0)
    sleep(0.1)

# Program choices
exog = True
forecasted = False
prediction_steps = 36
sim_old_day = True
if sim_old_day:
    # The date the simulation will "think" it is
    sim_date = datetime.now(pytz.utc).replace(year=2022, month=9, day=19)
    # sim_date = '2023-03-02 10:42:00 UTC'
    # timeOffset = datetime.now(pytz.utc) - parser.parse(sim_date)
    timeOffset = datetime.now(pytz.utc) - sim_date
else:
    timeOffset = None


mqttBroker ="mqtt.eclipseprojects.io"
client = mqtt.Client("Hoved_kontrol_2")
client.connect(mqttBroker)
client.subscribe(parameters.freezer_pow_request_2)
client.subscribe(parameters.ev_pow_request_2)
client.loop_start()
client.on_message=on_message


# region ZMQ Communication
context = zmq.Context()
f_in_socket = context.socket(zmq.SUB)
f_portin = parameters.freezer_port_2
f_in_socket.connect(f"tcp://{'localhost'}:{f_portin}")
f_in_socket.subscribe(parameters.freezer1_op_out_topic_2)

ev_in_socket = context.socket(zmq.SUB)
ev_portin = parameters.charger_port_2
ev_in_socket.connect(f"tcp://{'localhost'}:{ev_portin}")
ev_in_socket.subscribe(parameters.ev_use_topic_2)
# endregion

# region Logging
LOG_FILE = f'data/measurements/measurements_head_2_{time():.00f}.json'
print(f"Logging to file {LOG_FILE}")
# endregion

# getting usage data
#df_usage = prepData(filename='meterdata_fra_01-01_til_05-01.xls')
df_usage = prepData(filename='Meterdata_forbrug_20230329.ods')
# Setting up offset for simulation
#end_validation = '2023-02-08 00:00:00 UTC'
end_validation = '2023-05-01 21:00:00 UTC'

# Loading model
if exog:
    forecaster = load("modelExoTestKNN.pkl")
else:
    forecaster = load("model.pkl")


exogenous_vars = None
# Forecasting up to now, only run if don't have last 73 steps
if not sim_old_day:
    lastDate = parser.parse(end_validation)
    if exog:
        '''
        if sim_old_day:
            offset_time = (datetime.now(pytz.utc) - timeOffset).strftime('%Y-%m-%dT%H:00:00Z')
            ins = getMetObsIns(lastDate.strftime('%Y-%m-%dT%H:00:00Z'), endTime=offset_time)
        else:
            ins = getMetObsIns(lastDate.strftime('%Y-%m-%dT%H:00:00Z'))
        '''
        try:
            ins = getMetObsIns(lastDate.strftime('%Y-%m-%dT%H:00:00Z'))
            ins = ins.resample('H').mean()
        except:
            print('DMI API CAN NOT BE REACHED, SETTING INSOLATION TO 0')
            ins =pd.DataFrame(
                {'Hours': pd.date_range(lastDate, datetime.now(pytz.utc), freq='1H', closed='left')}
            )
            ins['insolation'] = 0
            ins = ins.rename(columns={'Hours' : 'HourUTC'})
            ins = ins.set_index('HourUTC')
            ins = addDateInfo(ins, dateColumn=ins.index)

        ins[['month','weekday','hour']] = ins[['month','weekday','hour']].astype(int)
        exogenous_vars = pd.get_dummies(ins, columns=['month', 'weekday','hour'])
        exogenous_vars = addMissingDummies(exogenous_vars)
        exogenous_vars = exogenous_vars.tail(-1)
    print(type(exogenous_vars.index))
    print(type(exogenous_vars.index[0]))
    precast = forecaster.predict(steps=len(exogenous_vars), exog = exogenous_vars).array
    exogenous_vars['amount'] = precast
    precast_df = exogenous_vars
    df_usage = df_usage.append(precast_df)


# Initial forecast
if exog:
    # Generating exogenous vars
    if sim_old_day:
        currTime = datetime.now(pytz.utc) - timeOffset
        diff = (df_usage.index - currTime).to_pytimedelta()
        maxval = np.amax(diff[(diff < pd.to_timedelta(0))])
        indexmax = np.where(diff == maxval)[0][0]
        last_window = df_usage.iloc[indexmax - 72:indexmax + 1].loc[:, 'amount']

        #currTime = datetime.now(pytz.utc) - timeOffset
        currTime = last_window.tail(1).index[0]
        nextTime = currTime + timedelta(hours=prediction_steps)
        try:
            exogenous_vars = getMetObsIns(startTime=currTime.strftime('%Y-%m-%dT%H:%M:%SZ'),
                                          endTime=nextTime.strftime('%Y-%m-%dT%H:%M:%SZ'))
            exogenous_vars = exogenous_vars.resample('H').mean()
        except:
            print('DMI API CAN NOT BE REACHED, SETTING INSOLATION TO 0')
            exo_index = last_window.index + timedelta(hours=73)
            exogenous_vars = pd.DataFrame(0, index=exo_index, columns=['insolation'])
            exogenous_vars = addDateInfo(exogenous_vars, dateColumn=exogenous_vars.index)
        # Counteracting variability in API return
        if exogenous_vars.head(1).index[0] == currTime:
            exogenous_vars = exogenous_vars.tail(-1)
        ins = exogenous_vars.loc[:, 'insolation']
        exogenous_vars = exogenous_vars[['month', 'weekday', 'hour']].astype(int)
        exogenous_vars['insolation'] = ins
    else:
        exogenous_vars = getForecastIns()
        # Forecast gives values for entire forecast model run, ensuring exo does not overlap with previous data
        hourDiff = int((df_usage.tail(1).index - exogenous_vars.head(1).index).seconds[0] / (60 * 60))
        exogenous_vars = exogenous_vars.tail(-(hourDiff + 1))
        last_window = df_usage['amount'].tail(73)
    exogenous_vars = pd.get_dummies(exogenous_vars, columns=['month', 'weekday', 'hour'])
    exogenous_vars = addMissingDummies(exogenous_vars)
    exogenous_vars = exogenous_vars.asfreq('H')


df_forecast = forecaster.predict(steps=prediction_steps, exog = exogenous_vars, last_window = last_window)


startTime = datetime.now()
while True:
    currTime = datetime.now(pytz.utc)-timeOffset
    # Generating power for real time simulation
    if (datetime.now()-startTime).seconds >= 10*60:
        old_usage = usage
        if sim_old_day:
            # Finding closest value in the past
            diff = (df_usage.index - currTime).to_pytimedelta()
            maxval = np.amax(diff[(diff < pd.to_timedelta(0))])
            indexmax = np.where(diff == maxval)[0][0]
            usage = abs(df_usage.iloc[indexmax].loc['amount'])
        else:
            try:
                usage = random.randint(0, 2000)
                usage = usage / 100
                usage = 1
            except:
                # Error handling for final implementation
                # Setting usage as 0
                print('Error reading current power, setting power to 0')
                usage = 0
        if old_usage == usage:
            usage_timeout += 1
            if usage_timeout>= 2:
                print('Getting repeating usage values, setting usage to 0 for safety')

        else:
            usage_timeout = 0
        if not (isinstance(usage, float) or isinstance(usage,int)):
            print('Invalid value for usage, setting as 0')
            usage = 0
        prevTime = currTime - timedelta(minutes=10)
        ins_Time = prevTime.strftime('%Y-%m-%dT%H:%M:%SZ')
        if sim_old_day:
            try:
                ins = getMetObsIns(startTime=ins_Time, endTime=currTime.strftime('%Y-%m-%dT%H:%M:%SZ'))
            except:
                try:
                    timeframe = (currTime-timedelta(minutes=60)).strftime('%Y-%m-%dT%H:%M:%SZ')+'/'+currTime.strftime('%Y-%m-%dT%H:%M:%SZ')
                    ins = getIns(timeframe=timeframe)
                except:
                    print('DMI API can not be reached, setting usage as export')
                    ins = pd.DataFrame(0, index=[currTime], columns=['insolation'])
        else:
            try:
                ins = getMetObsIns(startTime=ins_Time)
            except:
                print('DMI API can not be reached, setting usage as export')
                ins = pd.DataFrame(0, index=[currTime], columns=['insolation'])
        # If there is sun, power is exported
        try:
            if ins['insolation'][-1] >= 100:
                usage = -usage
        except:
            print(f'Error getting insolation: {ins}')
        ins['amount'] = usage
        if sim_old_day:
            tempIndex = df_usage.iloc[indexmax].name
            df_usage.loc[tempIndex,'amount'] = usage
        else:
            df_usage = df_usage.append(ins)
        startTime = datetime.now()
        print(f'Added usage: {usage}')

    # Sending current power usage
    if sim_old_day:
        diff = (df_usage.index - currTime).to_pytimedelta()
        maxval = np.amax(diff[(diff < pd.to_timedelta(0))])
        indexmax = np.where(diff == maxval)[0][0]
        pow = df_usage.iloc[indexmax].loc['amount']
    else:
        pow = df_usage['amount'][-1]
    ev_use = 0
    f_use = 0
    try:
        f_use = float(f_in_socket.recv_string(flags=zmq.NOBLOCK).split(';')[-1])
    except:
        print('Passing freezer')
    try:
        ev_use = float(ev_in_socket.recv_string(flags=zmq.NOBLOCK).split(';')[-1])
    except:
        print('Passing ev')
    pow += f_use+ev_use
    print(f'Current time {currTime}, current power {pow}; current energy usage {f_use+ev_use}')
    client.publish(f"{parameters.head_port_usage_topic_2}", pow)
    print(f"Published topic {parameters.head_port_usage_topic_2}: {pow}")

    if (datetime.now(pytz.utc) - timeOffset).hour == 21 and forecasted == False:
        try:
            if sim_old_day:
                diff = (df_usage.index - currTime).to_pytimedelta()
                maxval = np.amax(diff[(diff < pd.to_timedelta(0))])
                indexmax = np.where(diff == maxval)[0][0]
                last_window = df_usage.iloc[indexmax - 72:indexmax+1].loc[:, 'amount']
                last_window = last_window.asfreq('H')

                #currTime = datetime.now(pytz.utc) - timeOffset
                nowTime = last_window.tail(1).index[0]
                nextTime = nowTime + timedelta(hours=prediction_steps)
                try:
                    exogenous_vars = getMetObsIns(startTime=currTime.strftime('%Y-%m-%dT%H:%M:%SZ'),
                                                  endTime=nextTime.strftime('%Y-%m-%dT%H:%M:%SZ'))
                    exogenous_vars = exogenous_vars.resample('H').mean()
                except:
                    print('DMI API CAN NOT BE REACHED, SETTING INSOLATION TO 0')
                    exo_index = last_window.index + timedelta(hours=73)
                    exogenous_vars = pd.DataFrame(0, index=exo_index, columns=['insolation'])
                    exogenous_vars = addDateInfo(exogenous_vars, dateColumn=exogenous_vars.index)
                # Counteracting variability in API return
                if exogenous_vars.head(1).index[0] == nowTime:
                    exogenous_vars = exogenous_vars.tail(-1)
                ins = exogenous_vars.loc[:, 'insolation']
                exogenous_vars = exogenous_vars[['month', 'weekday', 'hour']].astype(int)
                exogenous_vars['insolation'] = ins
            else:
                exogenous_vars = getForecastIns()
                hourDiff = int((df_usage.tail(1).index - exogenous_vars.head(1).index).seconds[0] / (60 * 60))
                exogenous_vars = exogenous_vars.tail(-(hourDiff + 1))
                # Only keeping relevant part of df_usage
                df_usage = df_usage.tail(73)
                last_window = df_usage

            exogenous_vars = pd.get_dummies(exogenous_vars, columns=['month', 'weekday', 'hour'])
            exogenous_vars = addMissingDummies(exogenous_vars)
            exogenous_vars = exogenous_vars.asfreq('H')

            df_forecast = forecaster.predict(steps=prediction_steps, exog=exogenous_vars, last_window = last_window)
            print(f'New forecast!: {df_forecast}')
            forecasted = True
        except:
            print('ERROR TRYING TO PRODUCE NEW FORECAST, CREATING 0 FORECAST')
            df_forecast = df_usage.tail(24)
            df_forecast.index = df_forecast.index+timedelta(hours=-24)
            df_forecast['amount'] = 0

    if (datetime.now(pytz.utc)-timeOffset).hour == 20 and forecasted == True:
        forecasted = False

    # Sending forecast (1 hour ahead)
    currTime = datetime.now(pytz.utc)-timeOffset
    nextPow = df_forecast.loc[currTime.replace(microsecond=0,second=0, minute=0) + timedelta(hours=1)]
    client.publish(f"{parameters.head_port_forecast_topic_2}", nextPow)
    print(f"Published topic {parameters.head_port_forecast_topic_2}: {nextPow}")
    msg_df_string = df_forecast.to_csv()
    client.publish(f"{parameters.head_port_full_forecast_topic_2}", msg_df_string)
    print(f"Published topic {parameters.head_port_forecast_topic_2}: forecast string")
    # Writing data to log
    with open(LOG_FILE, 'a') as file:
        dump([f_use, ev_use, datetime.now(), pow, currTime], file, default=str)
        file.write('\n')
    #sleep(max(1-(datetime.now(pytz.utc)-currTime).seconds,0))
    sleep(1)

