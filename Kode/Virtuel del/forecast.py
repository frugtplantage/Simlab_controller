# Data processing
# ==============================================================================
import numpy as np
import pandas as pd
import os
import requests
from util import prepData


# Plots
# ==============================================================================
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.graphics.tsaplots import plot_pacf
import plotly.express as px

# Modelling and Forecasting
# ==============================================================================
from xgboost import XGBRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neural_network import MLPRegressor
#from lightgbm import LGBMRegressor
#from catboost import CatBoostRegressor

from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import train_test_split

from skforecast.ForecasterAutoreg import ForecasterAutoreg
from skforecast.ForecasterAutoregMultiOutput import ForecasterAutoregMultiOutput
from skforecast.model_selection import grid_search_forecaster
from skforecast.model_selection import backtesting_forecaster

from joblib import dump, load

# Configuration
# ==============================================================================
import warnings
#warnings.filterwarnings('ignore')
exo = True
plot = False
forecasting = True
forecast_filename = 'modelExoTestKNNLags.pkl'
predictions_filename = 'predsTemp.csv'

df_consump1 = prepData(filename='Meterdata_forbrug_20230329.ods')
df_consump2 = prepData(filename='Meterdata_fra_01-01_til_05-01.xls')
df_consump = pd.concat([df_consump1,df_consump2])
df_consump=df_consump[~df_consump.index.duplicated(keep='first')]
df_consump = df_consump.asfreq('H')

# Splitting into training, validation and testing sets
end_train = int(len(df_consump)*0.6)
end_validation = int(len(df_consump)*0.8)
data_train = df_consump.iloc[: end_train, :]
data_val   = df_consump.iloc[end_train:end_validation, :]
data_test  = df_consump.iloc[end_validation:, :]


print(f"Dates train      : {data_train.index.min()} --- {data_train.index.max()}  (n={len(data_train)})")
print(f"Dates validation : {data_val.index.min()} --- {data_val.index.max()}  (n={len(data_val)})")
print(f"Dates test       : {data_test.index.min()} --- {data_test.index.max()}  (n={len(data_test)})")

if plot:
    # Plot insolation v. usage
    fig, ax1 = plt.subplots()
    ax1.set_xlabel('X-axis')
    ax1.set_ylabel('Y1-axis', color='red')
    ax1.plot(data_test.index.values, data_test['amount'], color='red')
    ax1.tick_params(axis='y', labelcolor='red')
    ax2 = ax1.twinx()
    ax2.set_ylabel('Y2-axis', color='blue')
    ax2.plot(data_test.index.values, data_test['insolation'], color='blue')
    ax2.tick_params(axis='y', labelcolor='blue')
    plt.show()

    # Plot time series
    # ==============================================================================
    fig, ax = plt.subplots(figsize=(11, 4))
    data_train['amount'].plot(ax=ax, label='train')
    data_val['amount'].plot(ax=ax, label='validation')
    data_test['amount'].plot(ax=ax, label='test')
    ax.set_title('kWh')
    ax.legend()
    plt.show()

    # Boxplot chart for daily seasonality
    # ==============================================================================
    fig, ax = plt.subplots(figsize=(10, 3))
    mean_day_hour = df_consump.groupby(["weekday", "hour"])["amount"].mean()
    q25_day_hour = df_consump.groupby(["weekday", "hour"])["amount"].quantile(0.25)
    q75_day_hour = df_consump.groupby(["weekday", "hour"])["amount"].quantile(0.75)

    mean_day_hour.plot(ax=ax, label='mean')
    q25_day_hour.plot(ax=ax, linestyle='dashed', color='gray', label='')
    q75_day_hour.plot(ax=ax, linestyle='dashed', color='gray', label='quantile 25 and 75')

    ax.set(
    title="kWh used per hour",
    xticks=[i * 24 for i in range(7)],
    xticklabels=["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
    xlabel="Day",
    ylabel="kWh"
    )

    ax.legend()
    plt.show()

    # For hourly seasonality
    fig, ax = plt.subplots(figsize=(10, 3))
    mean_day_hour = df_consump.groupby(["hour"])["amount"].mean()
    q25_day_hour = df_consump.groupby(["hour"])["amount"].quantile(0.25)
    q75_day_hour = df_consump.groupby(["hour"])["amount"].quantile(0.75)

    mean_day_hour.plot(ax=ax, label='mean')
    q25_day_hour.plot(ax=ax, linestyle='dashed', color='gray', label='')
    q75_day_hour.plot(ax=ax, linestyle='dashed', color='gray', label='quantile 25 and 75')

    ax.set(
        title="kWh used per hour",
        xticks=[i for i in range(24)],
        xticklabels=[i for i in range(24)],
        xlabel="Hour",
        ylabel="kWh"
    )

    ax.legend()
    plt.show()

    # Autocorrelation plot
    # ==============================================================================
    fig, ax = plt.subplots(figsize=(7, 3))
    plot_acf(df_consump['amount'], ax=ax, lags=72)
    plt.show()

    # Partial autocorrelation plot
    # ==============================================================================
    fig, ax = plt.subplots(figsize=(7, 3))
    plot_pacf(df_consump['amount'], ax=ax, lags=72, method='ywm')
    plt.show()

'''
# Converting hour to cyclical variable
df_consump['hour_sin'] = np.sin(df_consump['hour'] / 23 * 2 * np.pi)
df_consump['hour_cos'] = np.cos(df_consump['hour'] / 23 * 2 * np.pi)
'''

# One-hot encoding
df_consump = pd.get_dummies(df_consump, columns=['month', 'weekday','hour'])
exogenous_vars = [column for column in df_consump.columns if column.startswith(('insolation', 'month', 'hour', 'weekday'))]

if forecasting:
    if exo:
        # Create forecaster
        # ==============================================================================
        forecaster = ForecasterAutoreg(
                        #regressor = XGBRegressor(random_state=321),
                        regressor = KNeighborsRegressor(),
                        #regressor = MLPRegressor(random_state=321),
                        lags = 24
                        )


        # Grid search of hyperparameters and lags
        # ==============================================================================
        # Regressor hyperparameters
        param_grid = {
            # For XGB
            #'n_estimators': [50, 100, 500],
            #'max_depth': [3, 5, 7],
            #'learning_rate': [0.01,0.1]
            # For KNN
            'n_neighbors': [3, 5, 7],
            'weights' : ['distance', 'uniform'],
            'p' : [1, 2]
            # For MLP
            #'learning_rate_init' : [0.001, 0.01],
            #'alpha' : [0.00005,0.0001,0.001]
            }
        # Lags used as predictors
        #lags_grid = [24, 48, 72, [1, 2, 3, 23, 24, 25, 71, 72, 73]]
        lags_grid = [[1, 2, 3, 23, 24, 25, 71, 72, 73]]

        results_grid = grid_search_forecaster(
                forecaster         = forecaster,
                y                  = df_consump.iloc[:end_validation].loc[:, 'amount'], # Train and validation data
                exog=df_consump.iloc[:end_validation].loc[:, exogenous_vars],
                param_grid         = param_grid,
                lags_grid          = lags_grid,
                steps              = 36,
                refit              = False,
                metric             = 'mean_squared_error',
                initial_train_size = int(len(data_train)), # Model is trained with training data
                fixed_train_size   = False,
                return_best        = True,
                verbose            = False
                )
        # Results of grid search
        # ==============================================================================
        results_grid.head(10)

        # Saving
        dump(forecaster, forecast_filename)

        # Backtesting test data
        # ==============================================================================
        metric, predictions = backtesting_forecaster(
            forecaster = forecaster,
            y          = df_consump['amount'],
            exog=df_consump[exogenous_vars],
            initial_train_size = len(df_consump.iloc[:end_validation]),
            fixed_train_size   = False,
            steps      = 36,
            refit      = False,
            metric     = 'mean_squared_error',
            verbose    = False # Change to True to see detailed information
            )
        print(f"Backtest error: {metric}")
        predictions.to_csv(predictions_filename)


    else:
        # Create forecaster
        # ==============================================================================
        forecaster = ForecasterAutoreg(
            regressor=XGBRegressor(random_state=123),
            lags=24
        )
        # Grid search of hyperparameters and lags
        # ==============================================================================
        # Regressor hyperparameters
        param_grid = {
            'n_estimators': [100, 500],
            #'n_estimators': [500],
            'max_depth': [3, 5, 10],
            #'max_depth': [3],
            'learning_rate': [0.01, 0.1]
            #'learning_rate': [0.01]
        }
        # Lags used as predictors
        lags_grid = [24, 48, 72, [1, 2, 3, 23, 24, 25, 71, 72, 73]]
        #lags_grid = [[1, 2, 3, 23, 24, 25, 71, 72, 73]]

        results_grid = grid_search_forecaster(
            forecaster=forecaster,
            y=df_consump.iloc[:end_validation].loc[:, 'amount'],  # Train and validation data
            param_grid=param_grid,
            lags_grid=lags_grid,
            steps=36,
            refit=False,
            metric='mean_squared_error',
            initial_train_size=int(len(data_train)),
            fixed_train_size=False,
            return_best=True,
            verbose=False
        )
        # Results of grid search
        # ==============================================================================
        results_grid.head(10)

        # Saving
        dump(forecaster, forecast_filename)

        # Backtesting test data
        # ==============================================================================
        metric, predictions = backtesting_forecaster(
            forecaster=forecaster,
            y=df_consump['amount'],
            initial_train_size=len(df_consump.iloc[:end_validation]),
            fixed_train_size=False,
            steps=36,
            refit=False,
            metric='mean_squared_error',
            verbose=True  # Change to True to see detailed information
        )
        print(f"Backtest error: {metric}")
        predictions.to_csv(predictions_filename)

    # Plot of predictions
    # ==============================================================================
    fig, ax = plt.subplots(figsize=(11, 4))
    data_test['amount'].plot(ax=ax, label='test')
    predictions['pred'].plot(ax=ax, label='predictions')
    ax.legend()
    plt.show()