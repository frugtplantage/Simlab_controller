# Data processing
# ==============================================================================
import numpy as np
import pandas as pd
import os
import requests
from util import prepData


# Plots
# ==============================================================================
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.graphics.tsaplots import plot_pacf
import plotly.express as px

# Modelling and Forecasting
# ==============================================================================
from xgboost import XGBRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neural_network import MLPRegressor
#from lightgbm import LGBMRegressor
#from catboost import CatBoostRegressor

from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import train_test_split

from skforecast.ForecasterAutoreg import ForecasterAutoreg
from skforecast.ForecasterAutoregMultiOutput import ForecasterAutoregMultiOutput
from skforecast.model_selection import grid_search_forecaster
from skforecast.model_selection import backtesting_forecaster

from joblib import dump, load

from sign_guesser import sign


exo = True
plot = False
forecasting = True
classifier_filename = 'classifier_sign.pkl'

df_consump = prepData()
df_consump['sign'] = df_consump['Mængde'].apply(lambda x:sign(x))
df_consump['Mængde'] = df_consump['Mængde'].apply(lambda x:abs(x))
df_consump['Mængde_prev'] = df_consump['Mængde'].shift(fill_value = 0)

# Splitting into training, validation and testing sets
end_train = '2022-12-08 00:00:00'
end_validation = '2023-02-08 01:00:00'
data_train = df_consump.loc[: end_train, :]
data_val   = df_consump.loc[end_train:end_validation, :]
data_test  = df_consump.loc[end_validation:, :]


print(f"Dates train      : {data_train.index.min()} --- {data_train.index.max()}  (n={len(data_train)})")
print(f"Dates validation : {data_val.index.min()} --- {data_val.index.max()}  (n={len(data_val)})")
print(f"Dates test       : {data_test.index.min()} --- {data_test.index.max()}  (n={len(data_test)})")


# Converting hour to cyclical variable
df_consump['hour_sin'] = np.sin(df_consump['hour'] / 23 * 2 * np.pi)
df_consump['hour_cos'] = np.cos(df_consump['hour'] / 23 * 2 * np.pi)

# One-hot encoding
df_consump = pd.get_dummies(df_consump, columns=['insolation', 'month', 'weekday'])

columns = [column for column in df_consump.columns if column.startswith(('Mængde','insolation', 'month', 'hour', 'weekday'))]
x_train = df_consump.loc[:end_validation, columns]
y_train = df_consump.loc[:end_validation,'sign']
x_test = df_consump.loc[end_validation:, columns]
y_test = df_consump.loc[end_validation:, 'sign']

clf = KNeighborsClassifier(n_neighbors=15, weights='uniform')
clf.fit(x_train,y_train)

preds = clf.predict(x_test)

correct = 0
incorrect = 0
j = 0
for i, row in y_test.iteritems():
    if row == preds[j]:
        correct += 1
    else:
        incorrect += 1
    j += 1
    try:
        pass
    except:
        print('ups')
print(correct)
print(incorrect)
print(correct/(incorrect+correct))

dump(clf, classifier_filename)
np.savetxt("sign_predictions.csv", preds, delimiter=",")