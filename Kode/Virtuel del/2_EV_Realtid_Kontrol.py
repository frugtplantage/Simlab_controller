import numpy as np
import pandas
import pandas as pd
import pytz

import parameters
from time import sleep, time, localtime
import paho.mqtt.client as mqtt
import zmq
from json import dump
from util import getPrices, getTariff
from math import ceil
from datetime import datetime
from io import StringIO

pow = 0
pow_time = datetime.now()
forecast = pd.DataFrame()
forecast_val = pd.DataFrame()
forecast_time = pd.DataFrame()
head_permission = 0
head_permission_time = datetime.now()
accessed_price_API = False
accessed_price_API_timer = datetime.now()


def on_message(client, userdata, message):
    sim_old_day = True
    client.publish(f"{parameters.ev_heartbeat_2}", chrg)
    global pow, pow_time, forecast_val, forecast_time, head_permission, head_permission_time, forecast
    if message.topic == parameters.head_port_usage_topic_2:
        pow = float(message.payload.decode("utf-8"))
        pow_time = datetime.now()
    if message.topic == parameters.head_port_full_forecast_topic_2:
        forecast_pre = str(message.payload.decode("utf-8"))
        forecast_pre = StringIO(forecast_pre)
        forecast_pre = pd.read_csv(forecast_pre)
        forecast_pre['Unnamed: 0'] = pd.to_datetime(forecast_pre['Unnamed: 0'], format='%Y-%m-%d %H:%M:%S', utc=True)
        forecast_pre = forecast_pre.rename(columns={'Unnamed: 0': 'HourUTC'})
        # Dårlig løsning til kørsel af gammel data
        if sim_old_day:
            offset = datetime.now(pytz.utc).replace(minute=0, second=0, microsecond=0) - forecast_pre.head(1)['HourUTC']
            for i, row in forecast_pre.iterrows():
                forecast_pre.at[i, 'HourUTC'] += offset[0]
        forecast_pre = forecast_pre.set_index('HourUTC')
        forecast = forecast_pre
    if message.topic == parameters.ev_pow_answer_2:
        head_permission = int(message.payload.decode("utf-8"))
        head_permission_time = datetime.now()
    sleep(0.5)


def chargingSchedule():
    global accessed_price_API
    price_Timeout = 0
    gotPrices = False
    while True:
        try:
            # Nordpool prices usually published at 13:45
            try:
                pricedf = getPrices()
            except:
                pricedf = getPrices(init=True)
            gotPrices = True
            print(f'Got the prices!')

        except:
            print("Can't access prices! Trying again")
            sleep(1)
            if price_Timeout >= 10:
                "Can't access prices, starting in fallback mode"
                break
            price_Timeout += 1
            continue
        break

    # Dropping rows to ensure charged by 6 am
    if gotPrices:
        for i, row in pricedf.iterrows():
            if i >= (pandas.Timestamp.now() + pandas.Timedelta(1, "d")).replace(hour=2, minute=00, second=00):
                pricedf = pricedf.drop(i)
        pricedf.index = pricedf.index.tz_localize('UTC')
        df_charge = pricedf.join([forecast])
        accessed_price_API = True
    else:
        print('Starting fallback mode, acting based on tarriffs and insolation')
        pricelist = []
        for i in range(23):
            pricelist.append(getTariff(i))
        pricelist.append(pricelist)
        df_charge = forecast
        df_charge['SpotPriceDKK'] = pricelist
        accessed_price_API = False
    # Removing nettariff if forecast expects surplus solar
    for i, row in df_charge.iterrows():
        # Removing an additional 0.05 because of sales price
        df_charge.at[i, 'SpotPriceDKK'] += (-getTariff(i.hour) - 0.05) * max(min(df_charge.loc[i][2] / (-ev_power), 1),
                                                                             0)

        # if -ev_power >= (1000 * df_charge.loc[i][2]):
        # Removing an additional 0.05 because of sales price
        # print(df_charge.loc[i][1])
        # df_charge.loc[i][1] += (-getTariff(i.hour) - 0.05)
        # print(df_charge.loc[i][1])

    while True:
        try:
            soc = float(in_socket.recv_string(flags=zmq.NOBLOCK).split(';')[-1])
            break
        except:
            print('SOC not accessible, trying again')
            sleep(1)
    chrgHrs = capacity * (max_SOC - soc) / (ev_power)
    # Sorting by price and designating the cheapest hours as charging hours
    df_charge = df_charge.sort_values(by='SpotPriceDKK', ascending=True)
    for i in range(ceil(chrgHrs)):
        df_charge.at[df_charge.index[i], 'Charge'] = 1
    print(df_charge)
    return df_charge


# If true, optimize for price, if false, optimize for
priceOpt = False
selfUseOpt = True

evTimeOut = 0
capacity = parameters.ev_capacity
ev_power = parameters.ev_power
min_SOC = 0.4
max_SOC = 0.8
chrg = 0
# This variable is how many kW current usage may vary from forecastet, and still charge
price_usage_sensitivity = 2

# # Communication
context = zmq.Context()
out_socket = context.socket(zmq.PUB)
portout = parameters.charger_controller_port_2
out_socket.bind(f"tcp://*:{portout}")

in_socket = context.socket(zmq.SUB)
portin = parameters.charger_port_2
in_socket.setsockopt(zmq.CONFLATE, 1)
in_socket.connect(f"tcp://{'localhost'}:{portin}")
in_socket.subscribe(parameters.charger_soc_topic_2)

mqttBroker = "mqtt.eclipseprojects.io"
client = mqtt.Client("evCharger_2")
client.connect(mqttBroker)
client.subscribe(parameters.head_port_usage_topic_2)
client.subscribe(parameters.head_port_full_forecast_topic_2)
client.subscribe(parameters.ev_pow_answer_2)
client.loop_start()
client.on_message = on_message

sleep(5)
# Logging
LOG_FILE = f'data/measurements/measurements_ev_2_{time():.00f}.json'
print(f"Logging to file {LOG_FILE}")

sleep(1)

# Initial charging schedule
df_charge = chargingSchedule()

try:
    while True:
        if (datetime.now() - pow_time).seconds > 60:
            pow = 0
            print('No new power received from head, setting power to 0')
            client.loop_stop(force=True)
            sleep(10)
            client.loop_start()
            client.on_message = on_message
            sleep(10)

        if not accessed_price_API and (datetime.now() - accessed_price_API_timer).seconds >= 30:
            df_charge = chargingSchedule()
            accessed_price_API_timer = datetime.now()
            print('Got the prices!')

        try:
            soc = float(in_socket.recv_string(flags=zmq.NOBLOCK).split(';')[-1])
            if evTimeOut >= 10:
                print('Calculating new charging schedule')
                df_charge = chargingSchedule()
            evTimeOut = 0
        except:
            print('EV not accessible, trying again in 10 seconds')
            sleep(10)
            if evTimeOut >= 10:
                print('EV is disconnected, calculating new charging schedule when reconnected')
                chrg = 0
            evTimeOut += 1
            continue
        print(f'Current power {pow} and soc {soc}')
        if soc <= min_SOC:
            chrg = 1
        elif soc <= max_SOC:
            if priceOpt:
                # Finding closest value in the past
                currDate = pandas.Timestamp.now()
                diff = df_charge['HourDK'] - currDate
                indexmax = (diff[(diff < pd.to_timedelta(0))].idxmax())
                if df_charge.loc[indexmax, 'Charge'] == 1:
                    if chrg == 1 and (pow - df_charge.loc[indexmax, 'pred']) < price_usage_sensitivity + ev_power:
                        pass
                    elif (pow - df_charge.loc[indexmax, 'pred']) < price_usage_sensitivity or df_charge.loc[
                        indexmax, 'pred'] > 0:
                        chrg = 1

                elif pandas.Timestamp.now().hour <= 4 and soc < max_SOC:
                    # Ensuring that vehicle will be charged in the morning
                    chrg = 1
                else:
                    chrg = 0
            elif selfUseOpt:
                if ((-ev_power > pow or (pow < 0 and chrg == 1)) and soc < max_SOC) and not (
                        head_permission and (datetime.now() - head_permission_time).seconds <= 30):
                    # Requesting power from head
                    print('Requesting to use surplus')
                    client.publish(f"{parameters.ev_pow_request_2}", ev_power)

                if (-ev_power > pow or (pow < 0 and chrg == 1)) and soc < max_SOC and (
                        head_permission and (datetime.now() - head_permission_time).seconds <= 60):
                    print('Got permission to use surplus power!')
                    chrg = 1
                elif pandas.Timestamp.now().hour <= 4 and soc < max_SOC:
                    # Ensuring that vehicle will be charged in the morning
                    chrg = 1
                else:
                    chrg = 0
            else:
                if soc < max_SOC:
                    chrg = 1

        else:
            chrg = 0
        print(f'Published {chrg}')
        out_socket.send_string(f"{parameters.charger_op_topic_2};{chrg}")
        with open(LOG_FILE, 'a') as file:
            dump([soc, chrg, pow, datetime.now()], file, default=str)
            file.write('\n')
        sleep(1)
except KeyboardInterrupt:
    out_socket.close()
    in_socket.close()
    client.loop_stop()
