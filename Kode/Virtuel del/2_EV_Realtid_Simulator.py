
import random
from time import sleep, time
import zmq
import parameters
from json import dump
from datetime import datetime


## Functions
def evDisconnectRand(soc):
    new_soc = max(soc-random.uniform(0,0.6),0)
    disrupt_time = random.randint(0.3*60*60,5*60*60)
    print(f"taking a drive for {disrupt_time/3600} hours, coming back with soc: {new_soc}")
    sleep(disrupt_time)
    return new_soc

def evDisconnect(soc):
    new_soc = soc-0.4
    disrupt_time = 2*60*60
    print(f"taking a drive for {disrupt_time/3600} hours, coming back with soc: {new_soc}")
    sleep(disrupt_time)
    return new_soc


## System values
# Capacity in J
capacity = parameters.ev_capacity
ev_power = parameters.ev_power
minCharge = 0.4
maxCharge = 0.9


## Program values
# initial SOC
soc = 0.5
charge = 0

# # Communication
context = zmq.Context()
out_socket = context.socket(zmq.PUB)
portout = parameters.charger_port_2
out_socket.bind(f"tcp://*:{portout}")
soctopic = parameters.charger_soc_topic_2

in_socket = context.socket(zmq.SUB)
portin = parameters.charger_controller_port_2
in_socket.connect(f"tcp://{'localhost'}:{portin}")
in_socket.subscribe(parameters.charger_op_topic_2)

LOG_FILE = f'data/measurements/measurements_charger_2_{time():.00f}.json'
print(f"Logging to file {LOG_FILE}")

while True:
    startTime = time()
    try:
        charge = float(in_socket.recv_string(flags=zmq.NOBLOCK).split(';')[-1])
        print(f'Received operation {charge}')
    except:
        pass
    if(charge == 1):
        soc += ev_power/(60*60) / capacity
    # Taking the car once every 1 hours on average
    #if(random.randint(0,1*60*60) == 1):
    #    soc = evDisconnectRand(soc)
    if datetime.now().hour == parameters.ev_disconnect_hour:
        soc = evDisconnect(soc)


    #Communication
    out_socket.send_string(f"{soctopic};{soc}")
    print(f"Published topic {soctopic}: {soc}")
    out_socket.send_string(f"{parameters.ev_use_topic_2};{str(charge * parameters.ev_power)}")
    print(f"Published topic {parameters.ev_use_topic_2}: {str(charge * parameters.ev_power)}")
    # Writing data to log
    with open(LOG_FILE, 'a') as file:
        dump([soc, charge, datetime.now()], file, default=str)
        file.write('\n')

    sleep(max(1-(time()-startTime),0))


